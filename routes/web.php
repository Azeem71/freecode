<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('welcome');
Route::post('/store', 'HomeController@store')->name('store');
// Route::get('/display', function () {
//     return view('display');
//});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('home', 'HomeController');
Route::resource('tourists', 'TouristController');
Route::get('tourist-login', 'Tourist\Auth\LoginController@showLoginForm')->name('tourist.login');
// Route::post('home', 'ToursController@update')->name('tours.update');

Route::post('tourist-login', 'Tourist\Auth\LoginController@login');
Route::get('tourist/home', 'TouristController@index')->name('tourist.home');
Route::get('tourist/tours', 'ToursController@search')->name('tourist.search');
// Route::get('tourist-register', 'Tourist\Auth\RegisterController@showLoginForm')->name('tourist.register');
Route::post('tourist-register', 'Tourist\Auth\RegisterController@register')->name('tourist.register');
Route::get('/display/{id}', 'ToursController@display')->name('display')->middleware('auth:tourist');

// $this->post('logout', 'Auth\LoginController@logout')->name('logout');


Route::post('create/tour', 'HomeController@createTour')->name('create.tour');


Route::post('/booking/save', 'ToursController@saveBooking')->name('booking.store');
Route::post('booking/edit', 'ToursController@editBooking')->name('booking.edit');;
Route::post('booking/delete', 'ToursController@deleteBooking')->name('booking.delete');

Route::post('/rating/save', 'RatingController@saveRating')->name('rate.now');


Route::post('/stripe', 'StripeController@payWithStripe')->name('stripe');


Route::get('/page-index/{id}', 'MessageController@index')->name('page.index');


//
//Route::get('/get-message', 'MessageController@getMessage')->name('get.message');
//Route::post('message-save', 'MessageController@sendMessage')->name('save.message');

Route::get('/message', 'MessageController@getMessage')->name('get-messages');
Route::post('message', 'MessageController@sendMessage')->name('store-messages');


Route::get('/guide-index/{id}', 'MessageGuideController@index')->name('guide.index');


//
//Route::get('/get-message', 'MessageController@getMessage')->name('get.message');
//Route::post('message-save', 'MessageController@sendMessage')->name('save.message');

Route::get('/message-guide', 'MessageGuideController@getMessage')->name('guide-get-messages');
Route::post('message-guide', 'MessageGuideController@sendMessage')->name('guide-get-messages');


Route::get('/redirect/{service}', 'SocialAuthController@redirect');
Route::get('/callback/{service}', 'SocialAuthController@callback');
