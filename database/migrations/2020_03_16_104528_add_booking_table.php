<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBookingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->bigInteger('guide_id')->unsigned();
            $table->foreign('guide_id')->references('id')->on('users');
            $table->bigInteger('booking_tour_id')->unsigned();
            $table->foreign('booking_tour_id')->references('tour_id')->on('tours');
            $table->bigInteger('booking_tourist_id')->unsigned();
            $table->foreign('booking_tourist_id')->references('id')->on('tourist_info');
            $table->integer('booking_by_person')->nullable();
            $table->integer('booking_by_group')->nullable();
            $table->integer('total_price_by_person')->nullable();
            $table->integer('total_price_by_group')->nullable();
            $table->string('booking_date')->nullable();
            $table->string('booking_start_time')->nullable();
            $table->string('booking_end_time')->nullable();
            $table->string('transaction_type')->nullable();
            $table->string('transaction_date')->nullable();
            $table->timestamps();
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking');
    }
}
