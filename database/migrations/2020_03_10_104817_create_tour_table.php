<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTourTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tours', function (Blueprint $table) {
            $table->bigIncrements('tour_id');
            $table->bigInteger('guide_id')->unsigned();
            $table->foreign('guide_id')->references('id')->on('users');
            $table->string('tour_name');
            $table->string('tour_city');
            $table->integer('tour_hours')->nullable();
            $table->integer('by_person')->nullable();
            $table->integer('by_group')->nullable();
            $table->integer('price_by_person')->nullable();
            $table->integer('price_by_group')->nullable();
            $table->longText('tour_description')->nullable();
            $table->string('tour_images')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tours');
    }
}
