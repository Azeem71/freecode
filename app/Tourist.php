<?php

namespace App;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Tourist extends Authenticatable
{
    use Notifiable;

    protected $table = 'tourist_info';
    //
    protected $fillable = [
        'first_name', 'last_name', 'email', 'sex', 'password', 'facebook_id', 'google_id'
    ];

    public function bookings()
    {
        return $this->hasMany(Booking::class, 'booking_tourist_id', 'id');
    }

    public function rating()
    {
        return $this->hasMany(Rating::class, 'tousist_id', 'id');
    }
}
