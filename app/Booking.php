<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Booking extends Model
{
    use Notifiable;

    protected $table = 'booking';
    //
    protected $guarded = [];


    public function customer()
    {
        return $this->belongsTo(Tourist::class, 'booking_tourist_id', 'id');
    }

    public function guide()
    {
        return $this->belongsTo(User::class, 'guide_id', 'id');
    }

    public function tour()
    {
        return $this->belongsTo(Tours::class, 'booking_tour_id', 'tour_id');
    }

    public function rating()
    {
        return $this->hasOne(Rating::class, 'booking_id', 'id');
    }
}
