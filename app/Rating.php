<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Rating extends Model
{
    use Notifiable;

    protected $table = 'ratings';
    //
    protected $guarded = [];


    public function customer()
    {
        return $this->belongsTo(Tourist::class, 'tousist_id', 'id');
    }
    public function tour()
    {
        return $this->belongsTo(Tours::class, 'tour_id', 'tour_id');
    }
    public function booking()
    {
        return $this->belongsTo(Tours::class, 'booking_id', 'id');
    }
}
