<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tours extends Model
{
    protected $table = 'tours';
    //
    protected $fillable = [
        'guide_id', 'tour_name', 'tour_city', 'tour_hours', 'tour_description', 'tour_images', 'by_group', 'by_person', 'price_by_group', 'price_by_person'
    ];

    public function bookings()
    {
        return $this->hasMany(Booking::class, 'booking_tour_id', 'tour_id');
    }

    public function ratings()
    {
        return $this->hasMany(Rating::class, 'tour_id', 'tour_id');
    }
}
