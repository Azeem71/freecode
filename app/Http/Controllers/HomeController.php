<?php

namespace App\Http\Controllers;

use App\Tours;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;
use Exception;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $tours = Tours::latest()->paginate(5);

        return view('home', compact('tours'))->with('i', (request()->input('page', 1) - 1) * 5);
        //return view('home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('home.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        var_dump($request->all());
        die();


        $request->validate([
            'tour_name' => 'required',
            'tour_hours' => 'required',
        ]);

        $name1 = array();


        if ($request->tour_images) {

            foreach ($request->tour_images as $image) {
                $name = rand() . '.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('/thumbnail');
                $img = Image::make($image->path());
                $img->resize(450, 450, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath . '/' . $name);
                $image->move(public_path('images'), $name);
                array_push($name1, $name);
            }

        }
        $img = json_encode($name1);

        // var_dump($request->tour_images);die;
        $count = 0;
        $result = DB::select('select count(guide_id) as total from tours where guide_id = ? ;', [$request->guide_id]);
        foreach ($result as $results) {
            $count = $results->total;
        }

        if ($count == 3) {

            //var_dump("hi");die;
            return ["error" => '4151', 'success' => null];
            //exit();
            //  return response()->json(['success' => 'Error msg'], 404);
            //throw new Exception("Sorry");
            // return redirect()->route('home.index')->with('Sorry','You cant create more than 3');

        } elseif ($request->select_payment == 'by_person') {
            Tours::create([
                'guide_id' => $request->guide_id,
                'tour_name' => $request->tour_name,
                'tour_city' => $request->tour_city,
                'tour_hours' => $request->tour_hours,
                'by_person' => $request->tour_persons,
                'price_by_person' => $request->tour_price,
                'tour_description' => $request->tour_description,
                'tour_images' => $img,
            ]);
            return ["error" => null, 'success' => '200'];
            // return redirect()->route('home.index')
            // ->with('success','Tour created successfully.');
        } elseif ($request->select_payment == 'by_group') {
            Tours::create([
                'guide_id' => $request->guide_id,
                'tour_name' => $request->tour_name,
                'tour_city' => $request->tour_city,
                'tour_hours' => $request->tour_hours,
                'by_group' => $request->tour_persons,
                'price_by_group' => $request->tour_price,
                'tour_description' => $request->tour_description,
                'tour_images' => $img,
            ]);
            return ["error" => null, 'success' => '200'];
            //    return redirect()->route('home.index')
            //    ->with('success','Tour created successfully.');
        }
        // Tours::create($request->all());


    }

    public function createTour(Request $request)
    {
        $request->validate([
            'tour_name' => 'required',
            'tour_hours' => 'required',
        ]);

        $name1 = array();


//        if ($request->tour_images) {
//
//            foreach ($request->tour_images as $image) {
//                $name = rand() . '.' . $image->getClientOriginalExtension();
//                $destinationPath = public_path('/thumbnail');
//                $img = Image::make($image->path());
//                $img->resize(450, 450, function ($constraint) {
//                    $constraint->aspectRatio();
//                })->save($destinationPath . '/' . $name);
//                $image->move(public_path('images'), $name);
//                array_push($name1, $name);
//            }
//
//        }
        if ($request->TotalImages > 0) {
            //Loop for getting files with index like image0, image1
            for ($x = 0; $x < $request->TotalImages; $x++) {

                if ($request->hasFile('images' . $x)) {

                    $file = $request->file('images' . $x);
                    $filename = $file->getClientOriginalName();
                    $extension = $file->getClientOriginalExtension();
                    $picture = date('His') . '-' . $filename;

                    //Save files in below folder path, that will make in public folder
                    $file->move(public_path('/thumbnail/images'), $picture);
                    array_push($name1, $picture);
                }
            }
        }


        $img = json_encode($name1);


        // var_dump($request->tour_images);die;
        $count = 0;
        $result = DB::select('select count(guide_id) as total from tours where guide_id = ? ;', [$request->guide_id]);
        foreach ($result as $results) {
            $count = $results->total;
        }

        if ($count == 3) {

            //var_dump("hi");die;
            return ["error" => '4151', 'success' => null];
            //exit();
            //  return response()->json(['success' => 'Error msg'], 404);
            //throw new Exception("Sorry");
            // return redirect()->route('home.index')->with('Sorry','You cant create more than 3');

        } elseif ($request->select_payment == 'by_person') {
            Tours::create([
                'guide_id' => $request->guide_id,
                'tour_name' => $request->tour_name,
                'tour_city' => $request->tour_city,
                'tour_hours' => $request->tour_hours,
                'by_person' => $request->tour_persons,
                'price_by_person' => $request->tour_price,
                'tour_description' => $request->tour_description,
                'tour_images' => $img,
            ]);
            return ["error" => null, 'success' => '200'];
            // return redirect()->route('home.index')
            // ->with('success','Tour created successfully.');
        } elseif ($request->select_payment == 'by_group') {
            Tours::create([
                'guide_id' => $request->guide_id,
                'tour_name' => $request->tour_name,
                'tour_city' => $request->tour_city,
                'tour_hours' => $request->tour_hours,
                'by_group' => $request->tour_persons,
                'price_by_group' => $request->tour_price,
                'tour_description' => $request->tour_description,
                'tour_images' => $img,
            ]);
            return ["error" => null, 'success' => '200'];
            //    return redirect()->route('home.index')
            //    ->with('success','Tour created successfully.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Tours $tours
     * @return \Illuminate\Http\Response
     */
    public function show(Tours $tours)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Tours $tours
     * @return \Illuminate\Http\Response
     */
    public function edit(Tours $tours)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Tours $tours
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tours $tours)
    {
        $request->validate([
            'tour_name_edit' => 'required',
            'tour_hours_edit' => 'required',
        ]);
        Tours::where('tour_id', $request->tour_id_edit)->update([
            'tour_name' => $request->tour_name_edit,
            'tour_city' => $request->tour_city_edit,
            'tour_hours' => $request->tour_hours_edit,
            'tour_description' => $request->tour_description_edit,
        ]);

        return redirect()->route('home.index')
            ->with('success', 'Tour updated successfully');
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Tours $tours
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Tours $tours)
    {

        Tours::where('tour_id', $request->tour_id_delete)->delete($request->all());
        return redirect()->route('home.index')
            ->with('success', 'Tour deleted successfully');
    }
}
