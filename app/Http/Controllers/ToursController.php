<?php

namespace App\Http\Controllers;

use App\Booking;
use App\Rating;
use App\Tours;
use Illuminate\Http\Request;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class ToursController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
//     public function __construct()
//     {
//         $this->middleware('auth');
//     }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $tours = Tours::latest()->paginate(5);

        return view('tourist/tours', compact('tours'))->with('i', (request()->input('page', 1) - 1) * 5);
        //return view('home');
    }

    public function search(Request $request)
    {
        $search = $request->search;
        $data = DB::table('tours')->where('tour_name', 'like', '%' . $search . '%')->paginate(5);
        return view('tourist/tours', ['tours' => $data]);
    }

    public function display($id)
    {
        $result = DB::select('select tours.*, users.*,Date(users.created_at) as member_since from tours Join users ON users.id=tours.guide_id where tour_id = ?', [$id]);

        $ratings = Rating::where('tour_id', $id)->get();
//        dd($result);
        return view('display', compact('result', 'ratings'))->with('id', $id);

    }


//    Booking functions

    public function saveBooking(Request $request)
    {

        $user = Auth::user();
        $response = array('status' => '', 'message' => "", 'data' => array());

//        $alreadyBooking = Booking::where('booking_date', $request->booking_date)->where('booking_end_time', $request->booking_end_time)->get();
        $alreadyBooking = Booking::where('booking_date', $request->booking_date)->where('booking_start_time', '>=', $request->booking_start_time)->where('booking_end_time', '<=', $request->booking_end_time)->get();


        $validator = Validator::make($request->all(), [
            'booking_date' => 'required',
            'booking_start_time' => 'required',
            'booking_end_time' => 'required',
            'no_of_peoples' => 'required',
        ]);

        if (!$validator->fails()) {
            if ($alreadyBooking->count() == 0) {
                $booking = new Booking();
                $booking->guide_id = $request->guide_id;
                $booking->booking_tour_id = $request->booking_tour_id;
                $booking->booking_tourist_id = $request->booking_tourist_id;
                $booking->booking_date = $request->booking_date;
                $booking->booking_start_time = $request->booking_start_time;
                $booking->booking_end_time = $request->booking_end_time;

                if ($request->type == 'group') {
                    $booking->total_price_by_group = $request->total_price_by_group;
                    $booking->booking_by_group = $request->no_of_peoples;
                } else {
                    $booking->total_price_by_person = $request->total_price_by_person;
                    $booking->booking_by_person = $request->no_of_peoples;
                }

                $booking->save();

                $response['status'] = 'success';
                $response['booking_id'] = $booking->id;
            } else {
                $response['status'] = 'already-booked';
            }


        } else {
            $response['status'] = 'error';
            $response['message'] = "Validation Errors.";
            $response['data'] = $validator->errors()->toArray();
        }

        return $response;


    }


    public function editBooking(Request $request)
    {

        $response = array('status' => '', 'message' => "", 'data' => array());

        $validator = Validator::make($request->all(), [
            'booking_start_time' => 'required',
            'booking_end_time' => 'required',

        ]);

        if (!$validator->fails()) {
            $booking = Booking::where('id', $request->booking_id)->first();
            $booking->booking_start_time = $request->booking_start_time;
            $booking->booking_end_time = $request->booking_end_time;
            $booking->save();

            $response['status'] = 'success';

        } else {
            $response['status'] = 'error';
            $response['message'] = "Validation Errors.";
            $response['data'] = $validator->errors()->toArray();
        }

        return $response;


    }

}
