<?php

namespace App\Http\Controllers;

use App\Tourist;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Laravel\Socialite\Facades\Socialite;

class SocialAuthController extends Controller
{
    public function redirect($service)
    {
        return Socialite::driver($service)->redirect();
    }

    public function callback($provider)
    {
        $userSocial = Socialite::driver($provider)->stateless()->user();
        if ($provider == 'facebook') {
            $finduser = Tourist::where('facebook_id', $userSocial->id)->first();
        } else {
            $finduser = Tourist::where('google_id', $userSocial->id)->first();
        }
        if ($finduser) {
            Auth::login($finduser);
            return Redirect::to('tourist/home');
        } else {
            $new_user = new Tourist();

            if ($provider == 'facebook') {
                $checkEmail = Tourist::where('email', $userSocial->email)->first();
                if ($checkEmail) {
                    return Redirect::to('tourist/home')->with([
                        'status' => 'error',
                        'message' => 'Email already exist.'
                    ]);
                } else {
                    $new_user->first_name = $userSocial->name;
                    $new_user->last_name = $userSocial->name;
                    $new_user->email = $userSocial->email;
                    $new_user->facebook_id = $userSocial->id;
                    $new_user->save();
                }
            } else {

                $checkEmail = Tourist::where('email', $userSocial->email)->first();
                if ($checkEmail) {
                    return Redirect::to('tourist/home')->with([
                        'status' => 'error',
                        'message' => 'Email already exist.'
                    ]);
                } else {
                    $new_user->first_name = $userSocial->name;
                    $new_user->last_name = $userSocial->name;
                    $new_user->email = $userSocial->email;
                    $new_user->google_id = $userSocial->id;
                    $new_user->save();
                }
            }
            Auth::login($new_user);
            return Redirect::to('tourist/home');


        }


    }
}
