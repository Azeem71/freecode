<?php

namespace App\Http\Controllers;


use App\Booking;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use Stripe\Stripe;
use Stripe\Customer;
use Stripe\Charge;

class StripeController extends Controller
{

    private $stripe_confg;

    public function __construct()
    {
        $this->stripe_confg = Config::get('stripe');
    }

    public function payWithStripe(Request $request)
    {
        $booking = Booking::where('id', $request->booking_id)->first();

        try {
            //dd($this->stripe_confg);
            Stripe::setApiKey($this->stripe_confg['stripe_secret']);

            $customer = Customer::create(array(
                'source' => $request->stripeToken
            ));
            $charge = Charge::create(array(
                'customer' => $customer->id,
                'amount' => ((!is_null($booking->booking_by_person)) ? $booking->total_price_by_person : $booking->total_price_by_group),
                'currency' => 'usd'
            ));

            if ($charge->status == 'succeeded') {
                $booking_transaction_date = Carbon::now();

                $booking->transaction_date = $booking_transaction_date->format('Y-m-d');
                $booking->transaction_type = 'stripe';
                $booking->save();
                $response['flash_status'] = 'success';

            }

        } catch (\Stripe\Exception\CardException $e) {
            // Since it's a decline, \Stripe\Exception\CardException will be caught
            echo 'Status is:' . $e->getHttpStatus() . '\n';
            echo 'Type is:' . $e->getError()->type . '\n';
            echo 'Code is:' . $e->getError()->code . '\n';
            // param is '' in this case
            echo 'Param is:' . $e->getError()->param . '\n';
            echo 'Message is:' . $e->getError()->message . '\n';
        } catch (\Stripe\Exception\RateLimitException $e) {
            // Too many requests made to the API too quickly
            $response['flash_status'] = 'error';
            $response['flash_message'] = $e->getMessage();

        } catch (\Stripe\Exception\InvalidRequestException $e) {
            // Invalid parameters were supplied to Stripe's API
            $response['flash_status'] = 'error';
            $response['flash_message'] = $e->getMessage();
        } catch (\Stripe\Exception\AuthenticationException $e) {
            // Authentication with Stripe's API failed
            $response['flash_status'] = 'error';
            $response['flash_message'] = $e->getMessage();
            // (maybe you changed API keys recently)
        } catch (\Stripe\Exception\ApiConnectionException $e) {
            // Network communication with Stripe failed
            $response['flash_status'] = 'error';
            $response['flash_message'] = $e->getMessage();
        } catch (\Stripe\Exception\ApiErrorException $e) {
            // Display a very generic error to the user, and maybe send
            $response['flash_status'] = 'error';
            $response['flash_message'] = $e->getMessage();
            // yourself an email
        } catch (\Exception $ex) {
            $response['flash_status'] = 'error';
            $response['flash_message'] = $ex->getMessage();
        }
        return $response;

    }
}
