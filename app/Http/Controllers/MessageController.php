<?php

namespace App\Http\Controllers;

use App\Events\MyEvent;
use App\Message;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Pusher\Pusher;
use Pusher\PusherException;

class MessageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:tourist');
//        $this->middleware('auth:tourist ');
    }

    public function index($id)
    {
//        $users = User::all();
        // count how many message are unread from the selected user
//        $users = DB::select("select users.id, users.name, users.avatar, users.email, count(is_read) as unread
//        from users LEFT  JOIN  messages ON users.id = messages.from and is_read = 0 and messages.to = " . $id . "
//        where users.id != " . $id . "
//        group by users.id, users.name, users.avatar, users.email");

        $users = DB::select("select users.id, users.name, users.avatar, users.email, count(is_read) as unread
        from tourist_info,users LEFT  JOIN  messages ON users.id = messages.from and is_read = 0 and messages.to = " . Auth::id() . "
        group by users.id, users.name, users.avatar, users.email");

        return view('messages.index', compact('users', 'id'));
    }

    public function getMessage(Request $request)
    {
        $my_id = Auth::id();

        // Make read all unread message
        Message::where(['from' => $request->id, 'to' => $my_id])->update(['is_read' => 1]);

        // Get all message from selected user
        $messages = Message::where(function ($query) use ($request, $my_id) {
            $query->where('from', $request->id)->where('to', $my_id);
        })->oRwhere(function ($query) use ($request, $my_id) {
            $query->where('from', $my_id)->where('to', $request->id);
        })->orderBy('created_at','asc')->get();

        return view('messages.message', ['messages' => $messages]);
    }

    public function sendMessage(Request $request)
    {
        $from = Auth::id();
        $to = $request->receiver_id;
        $message = $request->message;

        $data = new Message();
        $data->from = $from;
        $data->to = $to;
        $data->message = $message;
        $data->type = 'customer';
        $data->is_read = 0; // message will be unread when sending message
        $data->save();


        // pusher
        $options = array(
            'cluster' => 'ap2',
            // 'useTLS' => true
        );


        $pusher = new Pusher(
            '0ea14e6e7c66a09c4a7d',
            '94f08b9c86fed7b00b8e',
            '964717',
            $options
        );


        $data = ['from' => $from, 'to' => $to]; // sending from and to user id when pressed enter
        $pusher->trigger('my-channel', 'my-event', $data);


    }
}
