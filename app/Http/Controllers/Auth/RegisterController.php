<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'badge_number' => ['required', 'string', 'max:255'],
            'hours' => ['required', 'string', 'max:2'],
            'phone_number' => ['required', 'string', 'max:20'],
            'language' => ['required', 'string', 'max:255'],
            'sex' => ['required', 'string', 'max:255'],
            'avatar' => ['required', 'image', 'max:2048'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $image= $data['avatar'];
        $new_name =rand() . '.' .$image->getClientOriginalExtension();
        $destinationPath = public_path('/profile_images');
            $img = Image::make($image->path());
             $img->resize(800, 800, function ($constraint) {
             $constraint->aspectRatio();
             })->save($destinationPath.'/'.$new_name);
    //    $image->move(public_path('images'),$new_name);
        return User::create([
            'name' => $data['name'],
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'city' => $data['city'],
            'email' => $data['email'],
            'badge_number' =>$data['badge_number'],
            'hours' => $data['hours'],
            'phone_number' => $data['phone_number'],
            'language' =>$data['language'],
            'sex' => $data['sex'],
            'description' => $data['description'],
            'avatar' => $new_name,
            'password' => Hash::make($data['password']),
        ]);
    }
}
