<?php

namespace App\Http\Controllers;

use App\Rating;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RatingController extends Controller
{

    public function saveRating(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());

        $validator = Validator::make($request->all(), [
            'feedback' => 'required'
        ]);

        if (!$validator->fails()) {
            $rating = new Rating();
            $rating->rating = $request->rating;
            $rating->feedback = $request->feedback;
            $rating->tour_id = $request->tour_id;
            $rating->tousist_id = $request->tourist_id;
            $rating->booking_id = $request->booking_id;
            $rating->save();

            $response['status'] = 'success';

        } else {
            $response['status'] = 'error';
            $response['message'] = "Validation Errors.";
            $response['data'] = $validator->errors()->toArray();
        }

        return $response;

    }
}
