
<!DOCTYPE html>
<html>
<title>W3.CSS Template</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="/css/style.css">
<link rel="shortcut icon" href="assets/images/fav.png" type="image/x-icon">
  <meta name="description" content="">
<style>
body, h1,h2,h3,h4,h5,h6 {font-family: "Montserrat", sans-serif}
.w3-row-padding img {margin-bottom: 12px}
.bgimg {
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  background-image: url('/assets/images/Signup.jpg');
  min-height: 100%;
}

</style>
@extends('layouts.app')

@section('content')
<body>

 <div class="row">
<!-- Sidebar with image -->
<nav class="col-md-6 col-sm-6" style="position: fixed; width: 40% !important;height: 90%;margin-top: -18px !important;">
  <div class="bgimg"></div>
</nav>

<!-- Page Content -->
<div class="col-md-6" style="margin-left:40%">

  <!-- Menu icon to open sidebar -->
  
  <!-- Header -->
  <header class="w3-container w3-center" style="padding:10px 0px" id="home">
    <h1 class="w3-jumbo" style="font-weight:200;color:#7A97FF;font-size: 50px!important;"><b>Sign In</b></h1>
  </header>

    <div class="row">
  

</div>
    <!-- SignUp -->
      <form method="POST" action="{{ route('login') }}">
                        @csrf
      <p>
       <label for="name" style="color:#7A97FF" class="col-md-4 col-form-label text-md-left">{{ __('E-Mail Address') }}</label>
        <input id="email" type="email" class="w3-input w3-padding-16 w3-border @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

            @error('email')
            <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
            </span>
            @enderror
      </p>
     
       <p>
       <label for="name" style="color:#7A97FF" class="col-md-4 col-form-label text-md-left">{{ __('Password') }}</label>
        <input id="password" type="password" class="w3-input w3-padding-16 w3-border @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
         @error('password')
        <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
        </span>
         @enderror
      
      </p>
       <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>
      <div style="float:right;margin: 0em 2em 3em;">
        <button class="w3-button w3-light-grey w3-padding-large" type="submit">
           {{ __('Login') }}
        </button>
        </div>
  @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
      </p>
    </form>
  <!-- End Contact Section -->
  </div>  
  
 
<!-- END PAGE CONTENT -->
</div>
</div>
</body>
</html>
@endsection
