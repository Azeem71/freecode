
<!DOCTYPE html>
<html>
<title>W3.CSS Template</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="/css/style.css">
<link rel="shortcut icon" href="assets/images/fav.png" type="image/x-icon">
  <meta name="description" content="">
<style>
body, h1,h2,h3,h4,h5,h6 {font-family: "Montserrat", sans-serif}
.w3-row-padding img {margin-bottom: 12px}
.bgimg {
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  background-image: url('/assets/images/Signup.jpg');
  min-height: 100%;
}

</style>
@extends('layouts.app')

@section('content')
<body>

 <div class="row">
<!-- Sidebar with image -->
<nav class="col-md-6 col-sm-6" style="position: fixed; width: 40% !important;height: 90%;margin-top: -18px !important;">
  <div class="bgimg"></div>
</nav>

<!-- Page Content -->
<div class="col-md-6" style="margin-left:40%">

  <!-- Menu icon to open sidebar -->
  
  <!-- Header -->
  <header class="w3-container w3-center" style="padding:10px 0px" id="home">
    <h1 class="w3-jumbo" style="font-weight:200;color:#7A97FF;font-size: 50px!important;"><b>Sign Up</b></h1>
  </header>

    <!-- SignUp -->
      <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
                        @csrf
      <p>
       <label for="name" style="color:#7A97FF" class="col-md-4 col-form-label text-md-left">{{ __('User Name') }}</label>
      <input id="name" type="text" class="w3-input w3-padding-16 w3-border @error('name') is-invalid @enderror" name="name"  required autocomplete="name" autofocus>
       @error('name')
       <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
        </span>
        @enderror
      </p>
      <p>
       <label for="first_name" style="color:#7A97FF" class="col-md-4 col-form-label text-md-left">{{ __('First Name') }}</label>
      <input id="first_name" type="text" class="w3-input w3-padding-16 w3-border @error('first_name') is-invalid @enderror" name="first_name"  required autocomplete="first_name" autofocus>
       @error('first_name')
       <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
        </span>
        @enderror
      </p>
      <p>
       <label for="last_name" style="color:#7A97FF" class="col-md-4 col-form-label text-md-left">{{ __('Last Name') }}</label>
      <input id="last_name" type="text" class="w3-input w3-padding-16 w3-border @error('last_name') is-invalid @enderror" name="last_name"  required autocomplete="last_name" autofocus>
       @error('last_name')
       <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
        </span>
        @enderror
      </p>
      <p>
       <label for="city" style="color:#7A97FF" class="col-md-4 col-form-label text-md-left">{{ __('City') }}</label>
      <input id="city" type="text" class="w3-input w3-padding-16 w3-border @error('city') is-invalid @enderror" name="city"  required autocomplete="city" autofocus>
       @error('city')
       <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
        </span>
        @enderror
      </p>
      <p>
      <label for="name" style="color:#7A97FF" class="col-md-4 col-form-label text-md-left">{{ __('E-Mail Address') }}</label>
      <input id="email" type="email" class="w3-input w3-padding-16 w3-border @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
      
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
      </p>
      <p>
      <label for="badge_number" style="color:#7A97FF" class="col-md-4 col-form-label text-md-left">{{ __('Badge Number') }}</label>
      <input id="badge_number" type="text" class="w3-input w3-padding-16 w3-border @error('badge_number') is-invalid @enderror" name="badge_number" value="{{ old('badge_number') }}" required autocomplete="badge_number">
      
                                @error('badge_number')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
      </p>
      <p>
      <label for="hours" style="color:#7A97FF" class="col-md-4 col-form-label text-md-left">{{ __('Hours') }}</label>
      <input id="hours" type="text" class="w3-input w3-padding-16 w3-border @error('hours') is-invalid @enderror" name="hours" value="{{ old('hours') }}" required autocomplete="hours">
      
                                @error('hours')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
      </p>
      <p>
      <label for="phone_number" style="color:#7A97FF" class="col-md-4 col-form-label text-md-left">{{ __('Phone Number') }}</label>
      <input id="phone_number" type="text" class="w3-input w3-padding-16 w3-border @error('phone_number') is-invalid @enderror" name="phone_number" value="{{ old('phone_number') }}" required autocomplete="phone_number">
      
                                @error('phone_number')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
      </p>
      <p>
      <label for="language" style="color:#7A97FF" class="col-md-4 col-form-label text-md-left">{{ __('Language') }}</label>
      <select class="w3-input w3-padding-16 w3-border @error('language') is-invalid @enderror" id="language" name="language" required focus>
      <option value="Afrikaans" selected>Afrikaans</option>
  <option value="Albanian">Albanian</option>
  <option value="Arabic">Arabic</option>
  <option value="Armenian">Armenian</option>
  <option value="Basque">Basque</option>
  <option value="Bengali">Bengali</option>
  <option value="Bulgarian">Bulgarian</option>
  <option value="Catalan">Catalan</option>
  <option value="Cambodian">Cambodian</option>
  <option value="Chinese (Mandarin)">Chinese (Mandarin)</option>
  <option value="Croatian">Croatian</option>
  <option value="Czech">Czech</option>
  <option value="Danish">Danish</option>
  <option value="Dutch">Dutch</option>
  <option value="English">English</option>
  <option value="Estonian">Estonian</option>
  <option value="Fiji">Fiji</option>
  <option value="Finnish">Finnish</option>
  <option value="French">French</option>
  <option value="Georgian">Georgian</option>
  <option value="German">German</option>
  <option value="Greek">Greek</option>
  <option value="Gujarati">Gujarati</option>
  <option value="Hebrew">Hebrew</option>
  <option value="Hindi">Hindi</option>
  <option value="Hungarian">Hungarian</option>
  <option value="Icelandic">Icelandic</option>
  <option value="Indonesian">Indonesian</option>
  <option value="Irish">Irish</option>
  <option value="Italian">Italian</option>
  <option value="Japanese">Japanese</option>
  <option value="Javanese">Javanese</option>
  <option value="Korean">Korean</option>
  <option value="Latin">Latin</option>
  <option value="Latvian">Latvian</option>
  <option value="Lithuanian">Lithuanian</option>
  <option value="Macedonian">Macedonian</option>
  <option value="Malay">Malay</option>
  <option value="Malayalam">Malayalam</option>
  <option value="Maltese">Maltese</option>
  <option value="Maori">Maori</option>
  <option value="Marathi">Marathi</option>
  <option value="Mongolian">Mongolian</option>
  <option value="Nepali">Nepali</option>
  <option value="Norwegian">Norwegian</option>
  <option value="Persian">Persian</option>
  <option value="Polish">Polish</option>
  <option value="Portuguese">Portuguese</option>
  <option value="Punjabi">Punjabi</option>
  <option value="Quechua">Quechua</option>
  <option value="Romanian">Romanian</option>
  <option value="Russian">Russian</option>
  <option value="Samoan">Samoan</option>
  <option value="Serbian">Serbian</option>
  <option value="Slovak">Slovak</option>
  <option value="Slovenian">Slovenian</option>
  <option value="Spanish">Spanish</option>
  <option value="Swahili">Swahili</option>
  <option value="Swedish ">Swedish </option>
  <option value="Tamil">Tamil</option>
  <option value="Tatar">Tatar</option>
  <option value="Telugu">Telugu</option>
  <option value="Thai">Thai</option>
  <option value="Tibetan">Tibetan</option>
  <option value="Tonga">Tonga</option>
  <option value="Turkish">Turkish</option>
  <option value="Ukrainian">Ukrainian</option>
  <option value="Urdu">Urdu</option>
  <option value="Uzbek">Uzbek</option>
  <option value="Vietnamese">Vietnamese</option>
  <option value="Welsh">Welsh</option>
  <option value="Xhosa">Xhosa</option>
      </select>
      {{-- <input id="language" type="text" class="w3-input w3-padding-16 w3-border @error('language') is-invalid @enderror" name="language" value="{{ old('language') }}" required autocomplete="language"> --}}
      
                                @error('language')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
      </p>

      <p>
      <label for="sex" style="color:#7A97FF" class="col-md-4 col-form-label text-md-left">{{ __('Sex') }}</label>
      <select class="w3-input w3-padding-16 w3-border @error('sex') is-invalid @enderror" id="sex" name="sex" required focus>
      <option value="Male" selected>Male</option>
      <option value="Female">Female</option>
      </select>
      {{-- <input id="language" type="text" class="w3-input w3-padding-16 w3-border @error('language') is-invalid @enderror" name="language" value="{{ old('language') }}" required autocomplete="language"> --}}
      
                                @error('sex')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
      </p>
      <p>
      <label for="description" style="color:#7A97FF" class="col-md-4 col-form-label text-md-left">{{ __('Description') }}</label>
      <textarea rows="5" cols="50" id="description" name="description" class="w3-input w3-padding-16 w3-border @error('description') is-invalid @enderror" value="{{ old('description') }}" required autocomplete="description"></textarea>
      {{-- <input id="description" type="text" class="w3-input w3-padding-16 w3-border @error('description') is-invalid @enderror" name="description" value="{{ old('description') }}" required autocomplete="description"> --}}
      
                                @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
      </p>
      <p>
      <label for="avatar" style="color:#7A97FF" class="col-md-4 col-form-label text-md-left">{{ __('Avatar') }}</label>
      <input id="avatar" type="file" class="w3-input w3-padding-16 w3-border @error('avatar') is-invalid @enderror" name="avatar" value="{{ old('avatar') }}" required autocomplete="avatar">
      
                                @error('avatar')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
      </p>
       <p>
       <label for="name" style="color:#7A97FF" class="col-md-4 col-form-label text-md-left">{{ __('Password') }}</label>
       <input id="password" type="password" class="w3-input w3-padding-16 w3-border @error('password') is-invalid @enderror" name="password" value="{{ old('password') }}" required autocomplete="new-password">
      
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
      </p>
       <p>
       <label for="name" style="color:#7A97FF" class="col-md-4 col-form-label text-md-left">{{ __('Confirm Password') }}</label>
       <input id="password-confirm" type="password" class="w3-input w3-padding-16 w3-border @error('password') is-invalid @enderror" name="password_confirmation" value="{{ old('password') }}" required autocomplete="new-password">
      
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
      </p>
      
      <div style="float:right;margin: 0em 2em 3em;">
        <button class="w3-button w3-light-grey w3-padding-large" type="submit">
           {{ __('Register') }}
        </button>
        </div>
        Already have an account?  <a href="{{ route('login') }}" class="auth links">Sign In</a>
  
      </p>
    </form>
  <!-- End Contact Section -->
  </div>  
  
 
<!-- END PAGE CONTENT -->
</div>
</div>
</body>
</html>
@endsection
