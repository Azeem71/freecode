@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<html>
<title>W3.CSS Template</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="/css/style.css">
<style>
body, h1,h2,h3,h4,h5,h6 {font-family: "Montserrat", sans-serif}
.w3-row-padding img {margin-bottom: 12px}
.bgimg {
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  background-image: url('/assets/images/Signup.jpg');
  min-height: 100%;
}

</style>
<body>

 
<!-- Sidebar with image -->
<nav class="w3-sidebar w3-hide-medium w3-hide-small" style="width:40%">
  <div class="bgimg"></div>
</nav>

<!-- Page Content -->
<div class="w3-main w3-padding-large" style="margin-left:40%">
<div class="flex-center position-ref">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}" class="auth links">Sign In</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}" class="auth links">Register</a>
                        @endif
                    @endauth
                </div>
            @endif
</div>
  <!-- Menu icon to open sidebar -->
  
  <!-- Header -->
  <header class="w3-container w3-center" style="padding:10px 0px" id="home">
    <h1 class="w3-jumbo" style="font-weight:200;color:#7A97FF;font-size: 50px!important;"><b>Sign Up</b></h1>
  </header>

    <div class="row">
  <div class="column1">
    <img src="/assets/images/facebook.png" alt="facebook" style="width:100%">
  </div>
  <div class="column2">
    <img src="/assets/images/google.png" alt="google" style="width:100%">
  </div>

</div>
    <!-- SignUp -->
      <form method="POST" action="{{ route('register') }}">
                        @csrf
      <p><input id="name" class="w3-input w3-padding-16 w3-border @error('name') is-invalid @enderror" name="name"  required autocomplete="name" autofocus>
       @error('name')
       <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
        </span>
        @enderror
      </p>
      <p><input id="email" class="w3-input w3-padding-16 w3-border @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
      
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
      </p>
       <p><input id="password" class="w3-input w3-padding-16 w3-border @error('password') is-invalid @enderror" name="password" value="{{ old('password') }}" required autocomplete="new-password">
      
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
      </p>
       <p><input id="password-confirm" class="w3-input w3-padding-16 w3-border @error('password') is-invalid @enderror" name="password_confirmation" value="{{ old('password') }}" required autocomplete="new-password">
      
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
      </p>
      
      <div style="float:right;margin: 0em 2em 3em;">
        <button class="w3-button w3-light-grey w3-padding-large" type="submit">
           {{ __('Register') }}
        </button>
        </div>
        Already have an account?  <a href="{{ route('login') }}" class="auth links">Sign In</a>
  
      </p>
    </form>
  <!-- End Contact Section -->
  </div>  
  
 
<!-- END PAGE CONTENT -->
</div>
</body>
</html>
