@include('includes.messages')
    <!DOCTYPE html>
<html>
<head>

    <!-- Favicon below-->
    <link rel="shortcut icon" href="assets/images/fav.png" type="image/x-icon">
    <meta name="description" content="">

    <title>Home</title>
    <link rel="stylesheet" href="assets/w3school/style.css">
    <link rel="stylesheet" href="assets/googleapi/style.css">
    <link rel="stylesheet" href="assets/cdn/style.css">

    <link rel="stylesheet" href="assets/web/assets/mobirise-icons/mobirise-icons.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-reboot.min.css">
    <link rel="stylesheet" href="assets/tether/tether.min.css">
    <link rel="stylesheet" href="assets/socicon/css/styles.css">
    <link rel="stylesheet" href="assets/theme/css/style.css">
    <link href="assets/fonts/style.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link rel="preload" as="style" href="assets/mobirise/css/mbr-additional.css">
    <link rel="stylesheet" href="assets/mobirise/css/mbr-additional.css" type="text/css">
    <script src="js/script1.js"></script>

    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" rel="stylesheet">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Roboto', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
            height: 60px;
        }

        .bottom-left {
            position: absolute;
            bottom: 8px;
            left: 16px;
        }

        .right {
            position: absolute;
            top: 174px;
            right: 112px;
            width: 26%;

        }

        .top-left {
            position: absolute;
            top: 174px;
            left: 112px;
            width: 28%;
        }

        .bottom-right {
            position: absolute;
            bottom: 8px;
            right: 16px;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;

        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .centered1 {
            top: 61%;
            left: 51%;
            width: 37%;
            position: absolute;
            transform: translate(-50%, -50%);
        }

        .centered {
            top: 48%;
            left: 28%;
            width: 39%;
            position: absolute;
            transform: translate(-50%, -50%);
        }

        @media only screen and (min-width: 100px) and (max-width: 800px) {


            .top-left {
                position: absolute;
                top: 7px;
                left: 6px;
                width: 37%;
            }

            .right {
                position: absolute;
                top: 7px;
                right: 6px;
                width: 37%;
            }

            .display-7 {
                font-family: 'Rubik', sans-serif;
                font-size: 0.8rem;
            }

            .centered {
                top: 57%;
                left: 33%;
                width: 58%;
                position: absolute;
                transform: translate(-50%, -50%);
            }

            .centered1 {
                top: 57%;
                left: 51%;
                width: 30%;
                height: 200px;
                position: absolute;
                transform: translate(-50%, -50%);
            }

            .mbr-figure img, .card-img img {
                width: 100%;
                height: 279px;
            }


        }

        #myInput {
            background-image: url('assets/images/search.png');
            background-position: 5px 11px;
            background-size: 31px;
            background-repeat: no-repeat;
            width: 408px;
            font-size: 16px;
            padding: 12px 20px 12px 40px;
            border: 1px solid #ddd;
            margin-bottom: 12px;
        }

        .cid-rQBhelP6TA {
            padding-top: 90px;
            padding-bottom: 90px;
            background-color: #ddd;
        }

        #myUL {
            list-style-type: none;
            padding: 0;
            margin: 0;
        }

        #myUL li a {
            border: 1px solid #ddd;
            margin-top: -1px; /* Prevent double borders */
            background-color: #f6f6f6;
            padding: 12px;
            text-decoration: none;
            font-size: 18px;
            color: black;
            display: block
        }

        #myUL li a:hover:not(.header) {
            background-color: #eee;
        }

        .links > a:hover {
            padding: 10px 42px !important;
            border: 1px solid #7A97FF !important;
            color: #7A97FF !important;

        }

        .links > a {
            color: #636b6f;
            padding: 10px 42px !important;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;

        }

        .m-b-md {
            margin-bottom: 30px;
        }

        .mySlides {
            display: none;
        }

        /* Fading animation */
        .fade {
            -webkit-animation-name: fade;
            -webkit-animation-duration: 1.5s;
            animation-name: fade;
            animation-duration: 5.0s;


        }

        @-webkit-keyframes fade {
            from {
                opacity: .7
            }
            to {
                opacity: 1
            }
        }

        @keyframes fade {
            from {
                opacity: .7
            }
            to {
                opacity: 1
            }
        }

        h2 {
            font-size: 1.6rem !important;
        }

        @media screen and (max-width: 600px) {
            .links > a {
                color: #636b6f;
                padding: 10px 36px !important;
                font-size: 11px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }
        }


        .owl-nav button {
            position: absolute;
            top: 50%;
            background-color: #000;
            color: #fff;
            margin: 0;
            transition: all 0.3s ease-in-out;
        }

        .owl-nav button.owl-prev {
            left: 0;
        }

        .owl-nav button.owl-next {
            right: 0;
        }

        .owl-dots {
            text-align: center;
            padding-top: 15px;
        }

        .owl-dots button.owl-dot {
            width: 15px;
            height: 15px;
            border-radius: 50%;
            display: inline-block;
            background: #ccc;
            margin: 0 3px;
        }

        .owl-dots button.owl-dot.active {
            background-color: #000;
        }

        .owl-dots button.owl-dot:focus {
            outline: none;
        }

        .owl-nav button {
            position: absolute;
            top: 50%;
            transform: translateY(-50%);
            background: rgba(255, 255, 255, 0.38) !important;
        }

        span {
            font-size: 70px;
            position: relative;
            top: -5px;
        }

        .owl-nav button:focus {
            outline: none;
        }

        .owl-carousel .owl-stage {
            background-color: white;
        }

        .owl-carousel .owl-dots.disabled, .owl-carousel .owl-nav.disabled {
            display: block;
        }
    </style>

</head>
<body style="">

{{-- Sign In Popup Form start--}}
<div class="row">
    <div id="myModal" class="modalcustom">

        <!-- Modal content -->
        <div class="col-xs-6 modal-content">
            <span class="close" align="right">&times;</span>


            <!-- Sidebar with image -->
            <nav class="col-xs-3 mobile_change">

                <div class="bgimg"></div>
            </nav>

            <div class="col-xs-3" style="margin-left:39%">

                <!-- Menu icon to open sidebar -->

                <!-- Header -->
                <header class="w3-container w3-center" id="home">
                    <h1 class="w3-jumbo" style="font-weight:200;color:#7A97FF;font-size: 50px!important;"><b>Sign In</b>
                    </h1>
                </header>

                <div class="row">
                    <div class="column1">
                        <img src="/assets/images/facebook.png" alt="facebook" style="width:100%">
                    </div>
                    <div class="column2">
                        <img src="/assets/images/google.png" alt="google" style="width:100%">
                    </div>

                </div>
                <!-- SignIn -->
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <p>
                        <label for="name" style="color:#7A97FF"
                               class="col-xs-4 col-form-label text-xs-left">{{ __('E-Mail Address') }}</label>
                        <input id="email" type="email"
                               class="w3-input w3-padding-16 w3-border @error('email') is-invalid @enderror"
                               name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                        @error('email')
                        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
            </span>
                        @enderror
                    </p>

                    <p>
                        <label for="name" style="color:#7A97FF"
                               class="col-xs-4 col-form-label text-xs-left">{{ __('Password') }}</label>
                        <input id="password" type="password"
                               class="w3-input w3-padding-16 w3-border @error('password') is-invalid @enderror"
                               name="password" required autocomplete="current-password">
                        @error('password')
                        <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
        </span>
                        @enderror

                    </p>
                    <div class="form-group row">
                        <div class="col-xs-6 offset-xs-4">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="remember"
                                       id="remember" {{ old('remember') ? 'checked' : '' }}>

                                <label class="form-check-label" for="remember">
                                    {{ __('Remember Me') }}
                                </label>
                            </div>
                        </div>
                    </div>
                    <div style="width:25%;float:left">
                        <button class="w3-button w3-light-grey w3-padding-small"
                                style="padding: 10px !important;margin-top: 12%" type="submit">
                            {{ __('Login') }}
                        </button>
                    </div>
                    @if (Route::has('password.request'))
                        <a class="btn btn-link" href="{{ route('password.request') }}">
                            {{ __('Forgot Your Password?') }}
                        </a>
                    @endif

                </form>
                <!-- End Contact Section -->
            </div>


            <!-- END PAGE CONTENT -->
        </div>


    </div>
</div>

{{-- Sign In Popup Form end --}}


{{-- Sign Up Popup Form start--}}
<div class="row">
    <div id="myModal1" class="modalcustom">

        <!-- Modal content -->
        <div class="col-xs-6 modal-content">
            <span class="close" align="right">&times;</span>


            <!-- Sidebar with image -->
            <nav class="col-xs-3 mobile_change">

                <div class="bgimg"></div>
            </nav>

            <div class="col-xs-3" style="margin-left:39%;overflow-y:scroll">

                <!-- Menu icon to open sidebar -->

                <!-- Header -->
                <header class="w3-container w3-center" id="home">
                    <h1 class="w3-jumbo" style="font-weight:200;color:#7A97FF;font-size: 50px!important;"><b>Sign Up</b>
                    </h1>
                </header>

                <div class="row">
                    <div class="column1">
                        <img src="/assets/images/facebook.png" alt="facebook" style="width:100%">
                    </div>
                    <div class="column2">
                        <img src="/assets/images/google.png" alt="google" style="width:100%">
                    </div>

                </div>
                <!-- SignIn -->
                <form method="POST" action="{{ route('tourist.register') }}">
                    @csrf
                    <p>
                        <label for="first_name" style="color:#7A97FF"
                               class="col-xs-4 col-form-label text-md-left">{{ __('First Name') }}</label>
                        <input id="first_name" type="text"
                               class="w3-input w3-padding-16 w3-border @error('first_name') is-invalid @enderror"
                               name="first_name" required autocomplete="first_name" autofocus>
                        @error('name')
                        <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
        </span>
                        @enderror
                    </p>
                    <p>
                        <label for="last_name" style="color:#7A97FF"
                               class="col-xs-4 col-form-label text-md-left">{{ __('Last Name') }}</label>
                        <input id="last_name" type="text"
                               class="w3-input w3-padding-16 w3-border @error('last_name') is-invalid @enderror"
                               name="last_name" required autocomplete="last_name" autofocus>
                        @error('last_name')
                        <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
        </span>
                        @enderror
                    </p>
                    <p>
                        <label for="sex" style="color:#7A97FF"
                               class="col-md-4 col-form-label text-md-left">{{ __('Sex') }}</label>
                        <select class="w3-input w3-padding-16 w3-border @error('sex') is-invalid @enderror" id="sex"
                                name="sex" required focus>
                            <option value="Male" selected>Male</option>
                            <option value="Female">Female</option>
                        </select>
                        {{-- <input id="language" type="text" class="w3-input w3-padding-16 w3-border @error('language') is-invalid @enderror" name="language" value="{{ old('language') }}" required autocomplete="language"> --}}

                        @error('sex')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </p>
                    <p>
                        <label for="name" style="color:#7A97FF"
                               class="col-xs-4 col-form-label text-xs-left">{{ __('E-Mail Address') }}</label>
                        <input id="email" type="email"
                               class="w3-input w3-padding-16 w3-border @error('email') is-invalid @enderror"
                               name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                        @error('email')
                        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
            </span>
                        @enderror
                    </p>

                    <p>
                        <label for="name" style="color:#7A97FF"
                               class="col-xs-4 col-form-label text-xs-left">{{ __('Password') }}</label>
                        <input id="password" type="password"
                               class="w3-input w3-padding-16 w3-border @error('password') is-invalid @enderror"
                               name="password" required autocomplete="current-password">
                        @error('password')
                        <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
        </span>
                        @enderror

                    </p>

                    <p>
                        <label for="name" style="color:#7A97FF"
                               class="col-md-4 col-form-label text-md-left">{{ __('Confirm Password') }}</label>
                        <input id="password-confirm" type="password"
                               class="w3-input w3-padding-16 w3-border @error('password') is-invalid @enderror"
                               name="password_confirmation" value="{{ old('password') }}" required
                               autocomplete="new-password">

                        @error('password')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </p>

                    <div style="width: 25%;
    float: right;
    margin-right: 8%;
    margin-bottom: 2%;">
                        <button class="w3-button w3-light-grey w3-padding-small" style="padding: 10px !important"
                                type="submit">
                            {{ __('Register') }}
                        </button>
                    </div>
                    <div style="float:left">
                        Already have an account?

                        <a href="#" onclick="return signin()" class="auth links">Sign In</a></div>

                </form>
                <!-- End Contact Section -->
            </div>


            <!-- END PAGE CONTENT -->
        </div>


    </div>
</div>

{{-- Sign In Popup Form end --}}
<a href="{{ url('/') }}">
    <img src="assets/images/fav.png" alt="logo" style="width: 95px;
    margin-top: 0%;
    margin-bottom: -5%;">
</a>
<div class="flex-center position-ref">

    @if (Route::has('login'))
        <div class="top-right links">


            @auth('tourist')
                <a href="{{ url('tourist/home') }}">Dashboard</a>

            @elseauth
                <a href="{{ url('/home') }}">Dashboard</a>
            @else
                <a href="{{ route('register') }}" onclick="return register()" id="myBtn" class="auth links">Offer Your
                    Tours</a>
                <a href="#" onclick="return signin()" id="myBtn" class="auth links">Sign In</a>

                @if (Route::has('register'))
                    <a href="#" onclick="return register()" class="auth links">Register</a>
                @endif
            @endauth


        </div>
    @endif
</div>
<section class="cid-rQBbwpHPnN" id="image2-g">
    <figure class="mbr-figure">
        <div class="image-block" style="width: 100%;">
            {{-- <img src="assets/images/h3c.jpg" alt="Mobirise" title=""> --}}
            <div class="mySlides fade">
                <img src="assets/images/h1c.jpg" style="width:100%">
            </div>

            <div class="mySlides fade">
                <img src="assets/images/h2c.jpg" style="width:100%">
            </div>

            <div class="mySlides fade">
                <img src="assets/images/h3c.jpg" style="width:100%">
            </div>

            <div class="centered">
                <h1 class="card-title py-2 mbr-fonts-style align-center mbr-white display-5">No need to hide … with
                    Touristguide.ma find your next legal tour guide everywhere in Morocco</h1>
                <form action={{route('tourist.search')}} method="get">
                    <input type="text" id="myInput" name="search" onkeyup="myFunction()"
                           placeholder="Search by City or Tour" title="Type in a name">
                    <button class="btn btn-primary" type="submit">Search</button>
                </form>
            </div>
        </div>
    </figure>
</section>

<section class="engine"></section>
{{--<section class="features3 cid-rQMv9t5G7O" id="features3-n">--}}

{{--    <div class="container">--}}

{{--    </div>--}}

{{--</section>--}}


@php
    use Illuminate\Support\Facades\DB;
    $most_popular_services =DB::table('booking')
              ->select('booking_tour_id',DB::raw('COUNT(booking_tour_id) as total'))
              ->groupBy('booking_tour_id')
              ->orderBy('total', 'DESC')
              ->take(10)
              ->get();

    $most_popular_rating =DB::table('ratings')
              ->select('tour_id',DB::raw('SUM(rating) as total'))
              ->groupBy('tour_id')
              ->orderBy('total', 'DESC')
              ->take(10)
              ->get();


@endphp

<section style="margin-left: 58px; margin-right: 58px; background-color: white">
    <h3 class="mbr-section-title align-center mbr-fonts-style display-2" style="background-color: white">
        Most Popular Services</h3>
    <div class="owl-slider" style="margin-bottom: 49px; margin-top: 49px;">
        <div id="carousel" class="owl-carousel">
            @if(!empty($most_popular_services))
                @foreach($most_popular_services as $service)
                    @php
                        $tours =\App\Tours::where('tour_id',$service->booking_tour_id)->get();
                    @endphp
                    @if(!empty($tours))
                        @foreach($tours as $tour)
                            <a href="{{route('display', $tour->tour_id)}}">
                                <div class="item" style="border-style: solid;">

                                    <?php
                                    $p = array();
                                    $p = json_decode($tour->tour_images);
                                    ?>
                                    @foreach ((array)$p as  $images)
                                        @if(!empty($images))
                                            <img class="owl-lazy" height="300" width="100"
                                                 data-src="{{asset('thumbnail/images')}}/{{$images}}"
                                                 alt="">
                                        @endif
                                        @break;
                                    @endforeach

                                    <div style="margin-top: 25px;padding-left: 10px;">
                                        <h4 class="mbr-fonts-style display-7">
                                            {{ $tour->tour_name }}
                                        </h4>
                                        <p class="mbr-text mbr-fonts-style display-7">
                                            {{ \Illuminate\Support\Str::limit($tour->tour_description,100)}}.<br> City
                                            : {{ $tour->tour_city }}

                                        </p>
                                    </div>


                                </div>
                            </a>

                        @endforeach
                    @endif

                @endforeach
            @endif
        </div>

        {{--            @foreach($most_popular_services as $service)--}}
        {{--                {{ dd($service->booking_tour_id) }}--}}
        {{--                @foreach(\App\Tours::where('tour_id',$service->booking_tour_id)->get() as $tour)--}}
        {{--                    <div class="card p-3 col-12 col-md-6 col-lg-4 mouse_rove"--}}
        {{--                         onclick="window.location.replace('/display');">--}}
        {{--                        <div class="card-wrapper">--}}
        {{--                            <div class="card-img">--}}
        {{--                                <img src="assets/images/background1.jpg" alt="Mobirise">--}}
        {{--                            </div>--}}
        {{--                            <div class="card-box">--}}
        {{--                                <h4 class="card-title mbr-fonts-style display-7">--}}
        {{--                                    {{ $tour->tour_name }}--}}
        {{--                                </h4>--}}
        {{--                                <p class="mbr-text mbr-fonts-style display-7">--}}
        {{--                                    {{$tour->tour_description}}.<br> City : {{ $tour->tour_city }}--}}

        {{--                                </p>--}}
        {{--                            </div>--}}

        {{--                        </div>--}}
        {{--                    </div>--}}
        {{--                @endforeach--}}

        {{--            @endforeach--}}


    </div>
</section>

<section class="cid-rQBgbgnKn0" id="image2-i">


    <figure class="mbr-figure">
        <div class="image-block" style="width: 100%;">
            <img src="assets/images/why.jpg" alt="Mobirise" title="">
            <div class="centered1">
                <img src="assets/images/noir1.png" alt="Mobirise" title="">
            </div>
            <div class="top-left">
                <h2 class="card-title py-2 mbr-fonts-style align-center mbr-white display-5">Certified tour guide</h2>
                <p class="mbr-text mbr-fonts-style mbr-white display-7">All our tour guides are certified by the
                    minister of tourism ,and have a badge number. That's make us trustworthy.</p><br>
                <div>
                    <h2 class="card-title py-2 mbr-fonts-style align-center mbr-white display-5">Best offers</h2>
                    <p class="mbr-text mbr-fonts-style mbr-white display-7">You can compare between all the shown prices
                        to find the best offer for you. You have a lot of choices waiting for you.</p>
                </div>
            </div>


            <div class="right">
                <h2 class="card-title py-2 mbr-fonts-style align-center mbr-white display-5">Money back guarantee</h2>
                <p class="mbr-text mbr-fonts-style mbr-white display-7">Payment is realesed to the tourguide once you
                    enjoyed your tour and is satisfied with our services.</p><br>
                <div>
                    <h2 class="card-title py-2 mbr-fonts-style align-center mbr-white display-5">We are always here</h2>
                    <p class="mbr-text mbr-fonts-style mbr-white display-7">Hire best tour guide talent from around the
                        world on our best cIf you have any issue, our customer service can help you.</p>
                </div>
            </div>

        </div>
    </figure>
</section>

<section style="margin-left: 58px; margin-right: 58px; background-color: white">
    <h3 class="mbr-section-title align-center mbr-fonts-style display-2" style="background-color: white">
        Some Top Rated Services</h3>
    <div class="owl-slider" style="margin-bottom: 49px; margin-top: 49px;">
        <div id="carousel-slider" class="owl-carousel">
            @if(!empty($most_popular_rating))
                @foreach($most_popular_rating as $service)
                    @php
                        $tours =\App\Tours::where('tour_id',$service->tour_id)->get();
                    @endphp
                    @if(!empty($tours))
                        @foreach($tours as $tour)
                            <a href="{{route('display', $tour->tour_id)}}">
                                <div class="item" style="border-style: solid;">

                                    <?php
                                    $p = array();
                                    $p = json_decode($tour->tour_images);
                                    ?>
                                    @foreach ((array)$p as  $images)
                                        @if(!empty($images))
                                            <img class="owl-lazy" height="300" width="100"
                                                 data-src="{{asset('thumbnail/images')}}/{{$images}}"
                                                 alt="">
                                        @endif
                                        @break;
                                    @endforeach

                                    <div style="margin-top: 25px;padding-left: 10px;">
                                        <h4 class="mbr-fonts-style display-7">
                                            {{ $tour->tour_name }}
                                        </h4>
                                        <p class="mbr-text mbr-fonts-style display-7">
                                            {{ \Illuminate\Support\Str::limit($tour->tour_description,100)}}.<br> City
                                            : {{ $tour->tour_city }}

                                        </p>
                                    </div>


                                </div>
                            </a>
                        @endforeach
                    @endif
                @endforeach
            @endif
        </div>

        {{--            @foreach($most_popular_services as $service)--}}
        {{--                {{ dd($service->booking_tour_id) }}--}}
        {{--                @foreach(\App\Tours::where('tour_id',$service->booking_tour_id)->get() as $tour)--}}
        {{--                    <div class="card p-3 col-12 col-md-6 col-lg-4 mouse_rove"--}}
        {{--                         onclick="window.location.replace('/display');">--}}
        {{--                        <div class="card-wrapper">--}}
        {{--                            <div class="card-img">--}}
        {{--                                <img src="assets/images/background1.jpg" alt="Mobirise">--}}
        {{--                            </div>--}}
        {{--                            <div class="card-box">--}}
        {{--                                <h4 class="card-title mbr-fonts-style display-7">--}}
        {{--                                    {{ $tour->tour_name }}--}}
        {{--                                </h4>--}}
        {{--                                <p class="mbr-text mbr-fonts-style display-7">--}}
        {{--                                    {{$tour->tour_description}}.<br> City : {{ $tour->tour_city }}--}}

        {{--                                </p>--}}
        {{--                            </div>--}}

        {{--                        </div>--}}
        {{--                    </div>--}}
        {{--                @endforeach--}}

        {{--            @endforeach--}}


    </div>
</section>

<section class="features13 cid-rQBhelP6TA" id="features13-l">


    <div class="container">


        <div class="media-container-row">
            <div class="card col-12 col-md-6 align-center mouse_rove" onclick="return register()">
                <div class="card-wrap">
                    <div class="card-img">
                        <a href="#"><img src="assets/images/6-717x270.jpg" alt="Mobirise" title=""></a>
                    </div>
                    <div class="card-box p-4" style="padding:4% !important">
                        <h4 class="card-title py-2 mbr-fonts-style align-center mbr-white display-5">I Want To Become
                            <br> A Freelance Tour Guide</h4>
                        <p class="mbr-text mbr-fonts-style mbr-white display-7">
                            Guide tourists and get well paid thorugh<br> our platform
                        </p>
                    </div>
                </div>
            </div>
            <div class="card col-12 col-md-6 align-center  mouse_rove" onclick="window.location.replace('/display');">
                <div class="card-wrap">
                    <div class="card-img">
                        <a href="#"><img src="assets/images/6-717x270.jpg" alt="Mobirise" title=""></a>
                    </div>
                    <div class="card-box p-4">
                        <h4 class="card-title py-2 mbr-fonts-style align-center mbr-white display-5">I Want To Hire<br>
                            Guide</h4>
                        <p class="mbr-text mbr-fonts-style mbr-white display-7">Guide tourists and get well paid thorugh<br>
                            our platform</p>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section class="footer3 cid-rQBk90RhOx" id="footer3-m">


    <div class="container">
        <div class="media-container-row content">
            <div class="col-md-2 col-sm-4">
                <div class="mb-3 img-logo">
                    <a href="{{ url('/') }}">
                        <img src="assets/images/fav.png" style="width: 100px" alt="Mobirise">
                    </a>
                </div>

            </div>
            <div class="col-md-3 col-sm-4">
                <p class="mb-4 mbr-fonts-style foot-title display-7">
                    RECENT NEWS
                </p>
                <p class="mbr-text mbr-links-column mbr-fonts-style display-7">
                    <a href="#" class="text-black">About us</a>
                    <br><a href="#" class="text-black">Services</a>
                    <br><a href="#" class="text-black">Selected Work</a>
                    <br><a href="#" class="text-black">Get In Touch</a>
                    <br><a href="#" class="text-black">Careers</a>
                </p>
            </div>
            <div class="col-md-3 col-sm-4">
                <p class="mb-4 mbr-fonts-style foot-title display-7">
                    CATEGORIES
                </p>
                <p class="mbr-text mbr-fonts-style mbr-links-column display-7">
                    <a href="#" class="text-black">Business</a>
                    <br><a href="#" class="text-black">Design</a>
                    <br><a href="#" class="text-black">Real life</a>
                    <br><a href="#" class="text-black">Science</a>
                    <br><a href="#" class="text-black">Tech</a>
                </p>
            </div>
            <div class="col-md-4 col-sm-12">
                <p class="mb-4 mbr-fonts-style foot-title display-7"></p>
                <p class="mbr-text form-text mbr-fonts-style display-7"></p>


            </div>
        </div>
        <div class="footer-lower">
            <div class="media-container-row">
                <div class="col-sm-12">
                    <hr>
                </div>
            </div>
            <div class="media-container-row">
                <div class="col-sm-6 copyright">
                    <p class="mbr-text mbr-fonts-style display-7">
                        © Copyright 2020 Tour Guide - All Rights Reserved
                    </p>
                </div>
                <div class="col-md-6">

                </div>
            </div>
        </div>
    </div>
</section>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"
        type="text/javascript"></script>
<script>
    $("#carousel").owlCarousel({
        autoplay: true,
        lazyLoad: true,
        loop: false,
        margin: 20,
        /*
       animateOut: 'fadeOut',
       animateIn: 'fadeIn',
       */
        responsiveClass: true,
        autoHeight: true,
        autoplayTimeout: 7000,
        smartSpeed: 800,
        nav: true,
        responsive: {
            0: {
                items: 1
            },

            600: {
                items: 3
            },

            1024: {
                items: 4
            },

            1366: {
                items: 4
            }
        }
    });

    $("#carousel-slider").owlCarousel({
        autoplay: true,
        lazyLoad: true,
        loop: false,
        margin: 20,
        /*
       animateOut: 'fadeOut',
       animateIn: 'fadeIn',
       */
        responsiveClass: true,
        autoHeight: true,
        autoplayTimeout: 7000,
        smartSpeed: 800,
        nav: true,
        responsive: {
            0: {
                items: 1
            },

            600: {
                items: 3
            },

            1024: {
                items: 4
            },

            1366: {
                items: 4
            }
        }
    });
</script>
</body>
<script>
    function myFunction() {
        var input, filter, ul, li, a, i, txtValue;
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        ul = document.getElementById("myUL");
        li = ul.getElementsByTagName("li");
        for (i = 0; i < li.length; i++) {
            a = li[i].getElementsByTagName("a")[0];
            txtValue = a.textContent || a.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                li[i].style.display = "";
            } else {
                li[i].style.display = "none";
            }
        }
    }

    var slideIndex = 0;
    showSlides();

    function showSlides() {
        var i;
        var slides = document.getElementsByClassName("mySlides");
        //var dots = document.getElementsByClassName("dot");
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        slideIndex++;
        if (slideIndex > slides.length) {
            slideIndex = 1
        }
        /*for (i = 0; i < dots.length; i++) {
          dots[i].className = dots[i].className.replace(" active", "");
        }*/
        slides[slideIndex - 1].style.display = "block";
        //dots[slideIndex-1].className += " active";
        setTimeout(showSlides, 5000); // Change image every 2 seconds
    }

</script>
</html>
