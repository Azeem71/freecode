<div class="message-wrapper">
    <ul class="messages">
        @foreach($messages as $message)
            <li class="message clearfix" >
                {{--if message from id is equal to auth id then it is sent by logged in user --}}
                <div
                    class="{{ (($message->from ==  Auth::id()) && ($message->type == 'guider')) ? 'sent' : 'received' }}">
                    <p id="message_sent">{{ $message->message }}</p>
                    <p class="{{ (($message->from ==  Auth::id()) && ($message->type == 'guider')) ? 'date' : 'date-received' }}" id="date_sent">{{ date('d M y, h:i a', strtotime($message->created_at)) }}</p>
                </div>
            </li>
        @endforeach
    </ul>
</div>

<div class="input-text">
{{--    <input type="hidden" name="login_user" id="login_user" value="{{$user_id}}">--}}
    <input type="text" name="message" class="submit">
</div>
