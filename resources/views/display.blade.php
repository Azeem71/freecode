<head>
    @include('layouts.form')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" href="{{ asset('/css/toastr.min.css') }}">
    <style>
        @media screen and (max-width: 600px) {
            .links > a {
                color: #636b6f;
                padding: 10px 7px !important;
                font-size: 11px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }
        }

        /* Rating Star Widgets Style */
        .rating-stars {
            width: 100%;
        }

        .rating-stars ul {
            list-style-type: none;
            padding: 0;
            margin-top: 30px;

            -moz-user-select: none;
            -webkit-user-select: none;
        }

        .rating-stars ul > li.star {
            display: inline-block;

        }

        /* Idle State of the stars */
        .rating-stars ul > li.star > i.fa {
            font-size: 18px; /* Change the size of the stars */
            color: #ccc; /* Color on idle state */
        }

        /* Hover state of the stars */
        .rating-stars ul > li.star.hover > i.fa {
            color: #FFCC36;
        }

        /* Selected state of the stars */
        .rating-stars ul > li.star.selected > i.fa {
            color: #FF912C;
        }

        .ratingArea {
            margin-top: 30px;
        }

        .ratingArea h4 {
            font-weight: 700;
            margin: 10px 0;
            font-size: 16px;
        }

        .ratingArea img {
            object-fit: cover;
            width: 15%;
        }
    </style>


</head>
<body style="background-color:#FCFCFC;">


<a href="{{ url('/') }}">
    <img src="{{ asset('assets/images/fav.png')}}" alt="logo" style="width: 95px;
    margin-top: 0%;
    margin-bottom: -5%;">
</a>

<div class="flex-center position-ref">
    @if (Route::has('login'))
        <div class="top-right links">
            @auth
                <a href="{{ url('/') }}">Home</a>
            @else
                <a href="{{ url('/') }}" class="auth links">GuideTouristque</a>
                <a href="{{ route('register') }}" id="myBtn" class="auth links">Offer Your Tours</a>
                <a href="#" onclick="return signin()" class="auth links">Sign In</a>

                @if (Route::has('register'))
                    <a href="#" onclick="return register()" class="auth links">Register</a>
                @endif
            @endauth
        </div>
    @endif
</div>
<div class="container target">

    <br>
    <div class="row">
        <div class="col-md-9">
            <div class="topnav" id="myTopnav">
                <a href="#" class="active">Tour 1</a>
                <a href="#">Tour 2</a>
                <a href="#">Tour 3</a>
            </div>
            <br><br><br>
        </div>
        <div class="col-md-9">
            <div class="topnav" id="myTopnav">
                <a href="#overview" class="active">Tour Overview</a>
                <a href="#description">Description</a>
                <a href="#timing">Timings</a>
                <a href="#contact">Contact</a>
            </div>
            <br>
            @foreach ($result as $guide)
                <div id="overview" style="text-align:left;">
                    <h2> {{$guide->description}}</h2>
                </div>
                <br>
                <div style="width:30%">
                    <p class="profile_text">
                        <img class="profile_image" src="{{asset('profile_images')}}/{{$guide->avatar}}"
                             align="middle"> {{$guide->name}}
                    </p>
                    @if($ratings->count() > 0)
                        <div class='rating-stars'>
                            <ul id='stars'>

                                <li class='star     {{ (($ratings->sum('rating')/$ratings->count()) >= '1') ? 'selected' : '' }} '
                                    title='Poor'
                                    data-value='1'
                                    data-type="time">
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li class='star  {{ (($ratings->sum('rating')/$ratings->count()) >= '2') ? 'selected' : '' }}'
                                    title='Fair'

                                    data-value='2'
                                    data-type="time">
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li class='star   {{ (($ratings->sum('rating')/$ratings->count()) >= '3') ? 'selected' : '' }}'
                                    title='Good'

                                    data-value='3'
                                    data-type="time">
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li class='star  {{ (($ratings->sum('rating')/$ratings->count()) >= '4') ? 'selected' : '' }}'
                                    title='Excellent'

                                    data-value='4'
                                    data-type="time">
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li class='star  {{ (($ratings->sum('rating')/$ratings->count()) >= '5') ? 'selected' : '' }}'
                                    title='WOW!!!'

                                    data-value='5'
                                    data-type="time">
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                            </ul>
                        </div>
                    @endif
                </div>
                <!-- The four columns -->
                <div class="row">
                    <div class="container1 col-md-12">
                        <span onclick="this.parentElement.style.display='none'" class="closebtn">&times;</span>
                        <img id="expandedImg" src="{{ asset('assets/images/h1.jpg')}}" style="width:100%;height:400px">
                        <div id="imgtext">
                        </div>
                    </div>
                    <br>
                    <div class="column">
                        <?php
                        $p = array();
                        $p = json_decode($guide->tour_images);
                        ?>
                        @foreach ((array)$p as  $images)
                            @if(!empty($images))
                                <img src="{{asset('thumbnail/images')}}/{{$images}}" style="width:30%"
                                     onclick="picfunc(this);">
                            @endif
                        @endforeach
                    </div>
                </div>
                <br>

                <div id="description">
                    <h1>Description
                    </h1><br>
                    <p style="font-size: 17px;font-weight: 500;">
                        {{$guide->tour_description}}</p>
                </div>
                <br>
                <div id="timing">
                    <h1> Additional Information </h1>
                    <p style="font-size: 25px;font-weight: 500;">
                        Tour price : 100$/gourp ( Max 10person*)
                    </p>
                    <p style="font-size: 25px;font-weight: 500;">Duration of Tour: {{$guide->tour_hours}}.</p>
                </div>


                @if($ratings->count() > 0)
                    <div id="timing">
                        <h1> Reviews </h1>
                        <ul>

                            @foreach($ratings as $rating)
                                <li>{{ ucfirst($rating->feedback) }}</li>
                            @endforeach

                        </ul>

                    </div>
                @endif

                <br>
        </div>
        <div class="col-md-3">
            <a href="{{route('page.index', \Illuminate\Support\Facades\Auth::user()->id)}}" class="message_button">Message</a>
            <br> <br>

            <button type="button" class="book_button" data-toggle="modal" data-target="#bookNow">Book Now</button>

            <!-- Modal -->

            <div id="bookNow" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content" style="    height: 700px;">
                        <div class="modal-header">
                            <h4 class="modal-title">Add Booking Information</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>

                        </div>
                        <div class="modal-body">
                            <form id="booking-submit" method="post" action="javascript:void(0)">
                                @csrf
                                <input type="hidden" name="guide_id" value="{{ $guide->guide_id }}">
                                <input type="hidden" name="booking_tour_id" value="{{ $guide->tour_id }}">
                                <input type="hidden" name="booking_tourist_id"
                                       value="{{ \Illuminate\Support\Facades\Auth::user()->id }}">
                                <input type="hidden" name="tour_hours" id="booking_total_hours"
                                       value="{{ $guide->tour_hours }}">
                                @if(!is_null($guide->by_group))
                                    <input type="hidden" name="by_group" id="by_group"
                                           value="{{ $guide->by_group }}">
                                    <input type="hidden" name="price_by_group" id="price_by_group"
                                           value="{{ $guide->price_by_group }}">
                                    <input type="hidden" name="type" value="group">
                                @else
                                    <input type="hidden" name="by_person" id="by_person"
                                           value="{{ $guide->by_person }}">
                                    <input type="hidden" name="price_by_person" id="price_by_person"
                                           value="{{ $guide->price_by_person }}">
                                    <input type="hidden" name="type" value="person">
                                @endif

                                <div class="form-group">
                                    <label for="no_of_peoples">No Of Persons:</label>
                                    <input type="number" min="1" class="form-control" id="no_of_peoples"
                                           name="no_of_peoples" required
                                           data-type="{{ (!is_null($guide->by_group)) ? 'group' :'single' }}">
                                </div>
                                <div class="form-group">
                                    <label for="booking_date">Date:</label>
                                    <input type="text" class="form-control" id="booking_date" name="booking_date"
                                           required>
                                </div>
                                <div class="form-group">
                                    <label for="booking_start_time">Start Time:</label>
                                    <input type="text" class="form-control" id="booking_start_time" required
                                           name="booking_start_time">
                                </div>

                                <div class="form-group">
                                    <label for="booking_end_time">End Time:</label>
                                    <input type="text" class="form-control" id="booking_end_time" required
                                           name="booking_end_time">
                                </div>
                                @if(!is_null($guide->by_group))
                                    <div class="form-group">
                                        <label for="total_price_by_group">Total Cost:</label>
                                        <input type="number" class="form-control" id="total_price_by_group"
                                               value="0"
                                               readonly
                                               name="total_price_by_group">
                                    </div>
                                @else
                                    <div class="form-group">
                                        <label for="total_price_by_person">Total Cost:</label>
                                        <input type="number" class="form-control" id="total_price_by_person"
                                               value="0"
                                               readonly
                                               name="total_price_by_person">
                                    </div>
                                @endif
                                <div class="form-group">
                                    <button type="submit" id="send_form" class="btn btn-primary text-right">Submit
                                    </button>
                                </div>

                            </form>
                        </div>
                        <div class="modal-footer">
                            {{--                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
                        </div>
                    </div>

                </div>
            </div>

            <div id="payKnow" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content" style="    height: 700px;">
                        <div class="modal-header">
                            <h4 class="modal-title">Enter Your Credit Card Details</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>

                        </div>
                        <div class="modal-body">
                            <section class="price-area section-gap" id="price">
                                <div class="container">
                                    <div class="row">

                                        <div class="col-md-4 paymentImfo">
                                            <form method="POST" data-cc-on-file="false"
                                                  data-stripe-publishable-key="{{ env('STRIPE_KEY') }}"
                                                  id="payment-form">
                                                @csrf
                                                <input type="hidden" name="booking_id" id="booking_id" value="">
                                                <input type="hidden" name="booking_tourist_id"
                                                       value="{{ \Illuminate\Support\Facades\Auth::user()->id }}">
                                                <div class="row d-flex justify-content-center"
                                                     style="margin-top: 14px;">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <input type="text" name="name" id="card_name"
                                                                   placeholder="Card Name" class="form-control"
                                                                   required/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <input type="text" name="card_number" id="card_number"
                                                                   placeholder="Card Number" class="form-control"
                                                                   required/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <input type="text" name="cvv_number" id="cvc"
                                                                   placeholder="CVV Number" class="form-control"
                                                                   required/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                <div class="form-group">

                                                                    <select name="expiry_month" id="exp_month"
                                                                            class="form-control" required>
                                                                        <option value="" disabled>Expiry Month</option>
                                                                        <option value="01">01</option>
                                                                        <option value="02">02</option>
                                                                        <option value="03">03</option>
                                                                        <option value="04">04</option>
                                                                        <option value="05">05</option>
                                                                        <option value="06">06</option>
                                                                        <option value="07">07</option>
                                                                        <option value="08">08</option>
                                                                        <option value="09">09</option>
                                                                        <option value="10">10</option>
                                                                        <option value="11">11</option>
                                                                        <option value="12">12</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                <div class="form-group">

                                                                    <input type="text" name="expiry_year"
                                                                           id="expiry_year"
                                                                           placeholder="Expiry Year"
                                                                           class="form-control" required/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <button type="submit" id="payment-btn"
                                                                class="btn btn-primary float-right">
                                                            Pay Now
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>


                                </div>
                            </section>
                        </div>
                        <div class="modal-footer">
                            {{--                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
                        </div>
                    </div>

                </div>
            </div>

            <!-- Start price Area -->

            <!-- End price Area -->

            <br> <br>
            <!--right col-->
            <ul class="list-group">
                <li class="list-group-item text-muted" contenteditable="false">
                    <p class="profile_text">
                        <img class="profile_image" src="{{asset('profile_images')}}/{{$guide->avatar}}"
                             align="middle"> {{$guide->name}}
                    </p>
                    @if($ratings->count() > 0)
                        <div class='rating-stars'>
                            <ul id='stars'>

                                <li class='star     {{ (($ratings->sum('rating')/$ratings->count()) >= '1') ? 'selected' : '' }} '
                                    title='Poor'
                                    data-value='1'
                                    data-type="time">
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li class='star  {{ (($ratings->sum('rating')/$ratings->count()) >= '2') ? 'selected' : '' }}'
                                    title='Fair'

                                    data-value='2'
                                    data-type="time">
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li class='star   {{ (($ratings->sum('rating')/$ratings->count()) >= '3') ? 'selected' : '' }}'
                                    title='Good'

                                    data-value='3'
                                    data-type="time">
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li class='star  {{ (($ratings->sum('rating')/$ratings->count()) >= '4') ? 'selected' : '' }}'
                                    title='Excellent'

                                    data-value='4'
                                    data-type="time">
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li class='star  {{ (($ratings->sum('rating')/$ratings->count()) >= '5') ? 'selected' : '' }}'
                                    title='WOW!!!'

                                    data-value='5'
                                    data-type="time">
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                            </ul>
                        </div>
                    @endif
                </li>
                <li class="list-group-item text-left"><span class="pull-left"></span> Hire best tour guides talent from
                    around the world on our best,cost-efective and easy platform.
                </li>
                <li class="list-group-item text-left"><span class=""><strong
                            class="blue_text">General Info:</strong><br></span>
                    City: Germany<br>
                    Badge#: {{$guide->badge_number}}
                    <br>Language: {{$guide->language}}<br>
                    Member Since: {{$guide->member_since}}
                </li>
            </ul>
            <br>
        </div>
    </div>

</div>
@endforeach
<section class="footer3 cid-rQBk90RhOx" id="footer3-m">


    <div class="container" id="contact">
        <div class="media-container-row content">
            <div class="col-md-2 col-sm-4">
                <div class="mb-3 img-logo">
                    <a href="{{ url('/') }}">
                        <img src="{{ asset('assets/images/fav.png')}}" style="width:100px" alt="Mobirise">
                    </a>
                </div>

            </div>
            <div class="col-md-3 col-sm-4">
                <p class="mb-4 mbr-fonts-style foot-title display-7">
                    RECENT NEWS
                </p>
                <p class="mbr-text mbr-links-column mbr-fonts-style display-7">
                    <a href="#" class="text-black">About us</a>
                    <br><a href="#" class="text-black">Services</a>
                    <br><a href="#" class="text-black">Selected Work</a>
                    <br><a href="#" class="text-black">Get In Touch</a>
                    <br><a href="#" class="text-black">Careers</a>
                </p>
            </div>
            <div class="col-md-3 col-sm-4">
                <p class="mb-4 mbr-fonts-style foot-title display-7">
                    CATEGORIES
                </p>
                <p class="mbr-text mbr-fonts-style mbr-links-column display-7">
                    <a href="#" class="text-black">Business</a>
                    <br><a href="#" class="text-black">Design</a>
                    <br><a href="#" class="text-black">Real life</a>
                    <br><a href="#" class="text-black">Science</a>
                    <br><a href="#" class="text-black">Tech</a>
                </p>
            </div>
            <div class="col-md-4 col-sm-12">
                <p class="mb-4 mbr-fonts-style foot-title display-7"></p>
                <p class="mbr-text form-text mbr-fonts-style display-7"></p>


            </div>
        </div>
        <div class="footer-lower">
            <div class="media-container-row">
                <div class="col-sm-12">
                    <hr>
                </div>
            </div>
            <div class="media-container-row">
                <div class="col-sm-6 copyright">
                    <p class="mbr-text mbr-fonts-style display-7">
                        © Copyright 2020 Tour Guide - All Rights Reserved
                    </p>
                </div>
                <div class="col-md-6">

                </div>
            </div>
        </div>
    </div>
</section>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script
    src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/js/bootstrap-datetimepicker.min.js"></script>
<script src="{{ asset('js/script1.js')}}"></script>

<style>
    .bootstrap-datetimepicker-widget {

        width: 266px !important;
    }

</style>
<script type="text/javascript">
    $(function () {


        // Bootstrap DateTimePicker v4
        $('#expiry_year').datetimepicker({
            viewMode: 'years',
            format: 'YYYY',
            pickTime: false
        });
        $('#booking_date').datetimepicker({
            format: 'DD/MM/YYYY'
        });

        $('#booking_start_time').datetimepicker({

            format: "hh:mm A",
            pickDate: false
        });
        $('#booking_end_time').datetimepicker({

            format: "hh:mm A",
            pickDate: false
        });
    });

    $('#no_of_peoples').change(function (e) {
        e.preventDefault();
        if ($(this).data('type') === 'group') {
            if ($(this).val() <= $('#by_group').val()) {
                var total_group_cost = parseInt($('#price_by_group').val());
                $('#total_price_by_group').val(total_group_cost);
            } else {
                alert('Minimum group person is' + $('#by_group').val() + '');
                $(this).val('');
            }
        } else {
            if ($(this).val() >= $('#by_person').val()) {
                var total_person_cost = parseInt($(this).val()) * parseInt($('#price_by_person').val());
                $('#total_price_by_person').val(total_person_cost);
            } else {
                alert('Minimum person is' + $('#by_person').val() + '');
                $(this).val('');
            }
        }
    });
    $('#booking-submit').submit(function (e) {
        e.preventDefault();

        var start_time = moment($("#booking_start_time").val(), "HH:mm a");
        var end_time = moment($("#booking_end_time").val(), "HH:mm a");
        var total_hours = end_time.diff(start_time, 'hours');
        var isGreater = end_time.isAfter(start_time, 'hours');

        if (isGreater) {
            if (total_hours <= parseInt($('#booking_total_hours').val())) {
                let form = $('#booking-submit');
                var request = $(form).serialize();

                $.ajax({
                    type: "POST",
                    url: "{{ route('booking.store') }}",
                    data: request,
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        if (response.status == 'success') {

                            alert('booking save successfully');
                            $("#bookNow").modal('hide');
                            $('#booking_id').val(response.booking_id);
                            $("#payKnow").modal('show');
                        } else if (response.status == 'already-booked') {
                            alert('We has Already Booking For this tour');
                        } else {
                            alert('Please fill all required fields.');
                        }

                    },
                    error: function () {
                        alert('Something went wrong.');
                    }
                });
            } else {
                alert('You have exceed tour hours limit ' + parseInt($('#booking_total_hours').val()) + '');
            }
        } else {
            alert('End Time Should be Greater than start Time');
        }


    });
</script>


<script src="https://js.stripe.com/v2/"></script>
<script>

    // // Create a Stripe client.
    Stripe.setPublishableKey("pk_test_wpkJWqempchduQBeT80yxPpL00JQRMawKX");
    // //var stripe = Stripe('pk_test_16K7zw7Vw7Iyd5xjNBb9MUXS');

    $(document).ready(function () {
        $('#card_number').click(function () {
            $(this).val('');
        });

        $('#payment-form').submit(function (event) {
            $('#payment-btn').val('Processing...');

            var $form = $(this);

            // Stripe.setPublishableKey($form.data('stripe-publishable-key'));
            Stripe.card.createToken({
                number: $('#card_number').val(),
                cvc: $('#cvc').val(),
                exp_month: $('#exp_month').val(),
                exp_year: $('#expiry_year').val(),
                name: $('#card_name').val(),
            }, stripeResponseHandler);
            return false; // submit from callback
        });
    });

    function stripeResponseHandler(status, response) {
        if (response.error) {

            $(".payment-errors").html(response.error.message);
            $(".payment-errors").addClass('alert alert-danger');
            $("html, body").animate({scrollTop: 0}, "slow");
        } else {

            var form_id = $("#payment-form");
            // token contains id, last4, and card type
            var token = response['id'];
            // insert the token into the form so it gets submitted to the server
            form_id.append("<input type='hidden' name='stripeToken' value='" + token + "' />");
            // and submit

            let form = $('#payment-form');
            var request = $(form).serialize();

            $.ajax({
                type: "POST",
                url: "{{ route('stripe') }}",
                data: request,
                dataType: "json",
                cache: false,
                success: function (response) {
                    if (response.flash_status == 'success') {

                        alert('Payment Done successfully');
                        $("#payKnow").modal('hide');
                        window.location.href = '{{ route('tourist.home') }}';
                    } else if (response.flash_status == 'error') {
                        alert(response.flash_message);
                    }

                },
                error: function () {
                    alert('Something went wrong.');
                }
            });


            // document.getElementById('payment-form').submit();
        }
    }
</script>
<script src="{{ asset('/js/toastr.min.js') }}"></script>
<script>
        @if(Session::has('flash_message'))
    var type = "{{ Session::get('flash_status') }}";
    switch (type) {
        case 'info':
            toastr.info("{{ Session::get('flash_message') }}");
            break;

        case 'warning':
            toastr.warning("{{ Session::get('flash_message') }}");
            break;

        case 'success':
            toastr.success("{{ Session::get('flash_message') }}");
            break;

        case 'error':
            toastr.error("{{ Session::get('flash_message') }}");
            break;
    }
    @endif
</script>

</body>

