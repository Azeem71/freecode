<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="{{ asset('/css/style.css')}}">
<link rel="stylesheet" href="{{ asset('/js/script1.js')}}">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="shortcut icon" href="{{ asset('assets/images/fav.png')}}" type="image/x-icon">
  <meta name="description" content="">
<style>
#myInput {
  background-image: url("{{ asset('assets/images/search.png') }}") ;
 /* background-image: url('assets/images/search.png');*/
 background-position: 5px 11px;
    background-size: 31px;
    background-repeat: no-repeat;
    width: 380px;
  font-size: 16px;
  padding: 12px 20px 12px 40px;
  border: 1px solid #ddd;
  margin-bottom: 12px;
}
.tours {
    margin-left: 1%;
    height: 240px;
    width: 85%;
    margin-top: 25px;
    border-color: #5bbaff;
    -webkit-box-shadow: 0 0 10px #5bbaff;
    box-shadow: 0 0 10px #5bbaff;
    background-color:white;
}
.tours:hover {
    margin-left: 1%;
    height: 240px;
    width: 85%;
    margin-top: 25px;
    border-color: #5bff77 !important;
    -webkit-box-shadow: 0 0 10px #5bff77 !important;
    box-shadow: 0 0 10px #5bff77 !important;
    background-color:white;
}
a{
  color:black !important;
}
b{
  margin-left:20px;
}
</style>
</head>
<body style="background-color: whitesmoke;">
 <div class="flex-center position-ref" style="
    background-color: black;">


                <div class="top-right links" >

                        <a style="
    color: white !important;" href="{{ url('/') }}">Home</a>
</div>

</div>
<form style="margin-top: 20px;margin-left: 20px;" action={{route('tourist.search')}} method="get">
            <input type="text" id="myInput" name="search" onkeyup="myFunction()" placeholder="Search by City or Tour" title="Type in a name">
            <button class="btn btn-primary" type="submit" style="width: 114px !important;height: 52px !important;">Search</button>
            </form>
 @foreach ($tours as $tour)
                <a href="{{route('display', $tour->tour_id)}}">
               <div class="tours">
                <?php
               $p=array();
               $p= json_decode($tour->tour_images);
                ?>
                @foreach ((array)$p as  $images)
                @if(!empty($images))
                 <img class="tour_image" src="{{asset('thumbnail/images')}}/{{$images}}"  align="left">
                 @endif
                @break;
                @endforeach
              <div align="centre">
                <p style="">
                <br>
                <b>Tour Name</b><br><span style="margin-left:20px;">{{ $tour->tour_name }}</span><br><br>
                <b>Description</b><br><span style="margin-left:20px;">{{ $tour->tour_description }}</span><br><br>
                <b>Tour Time</b><br><span style="margin-left:20px;">{{ $tour->tour_hours }}</span><br><br>
              </p>
              </div>
               </div>
                </a>
               @endforeach
               {!! $tours->links() !!}

</body>
</html>
