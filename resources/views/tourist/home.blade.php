@extends('layouts.app')
@push('css')



    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href=" https://cdn.datatables.net/fixedheader/3.1.6/css/fixedHeader.bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" href="{{ asset('/css/toastr.min.css') }}">
    <style>
        /* Rating Star Widgets Style */
        .rating-stars {
            width: 100%;
        }

        .rating-stars ul {
            list-style-type: none;
            padding: 0;
            margin-top: 30px;

            -moz-user-select: none;
            -webkit-user-select: none;
        }

        .rating-stars ul > li.star {
            display: inline-block;

        }

        /* Idle State of the stars */
        .rating-stars ul > li.star > i.fa {
            font-size: 18px; /* Change the size of the stars */
            color: #ccc; /* Color on idle state */
        }

        /* Hover state of the stars */
        .rating-stars ul > li.star.hover > i.fa {
            color: #FFCC36;
        }

        /* Selected state of the stars */
        .rating-stars ul > li.star.selected > i.fa {
            color: #FF912C;
        }

        .ratingArea {
            margin-top: 30px;
        }

        .ratingArea h4 {
            font-weight: 700;
            margin: 10px 0;
            font-size: 16px;
        }

        .ratingArea img {
            object-fit: cover;
            width: 15%;
        }
    </style>
@endpush
@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">

                {{--                <div class="card-header">Dashboard {{ Auth::user()->first_name }} </div>--}}
                <a href="{{ route('tourist.search') }}" class="btn btn-primary" style="margin-bottom: 21px;">Book New
                    Tours</a>


                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif


                <span style="float: right;margin-bottom: 25px;"><strong>Total Bookings: </strong>  {{ $user->bookings->count()    }}</span>
                <table id="example" class="table table-striped table-bordered  table-responsive" style="width:100%">
                    <thead>
                    <tr>
                        <th>Guider Name</th>
                        <th>Tour Name</th>
                        <th>Tour City</th>
                        <th>By Person</th>
                        <th>Total Price by Person</th>
                        <th>By Group</th>
                        <th>Total Price by Group</th>
                        <th>Booking Date</th>
                        <th>Start Time</th>
                        <th>End Time</th>
                        <th>Status</th>
                        <th style="    width: 116px;">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($user->bookings as $booking)
                        @if(!is_null($booking->tour))
                            <tr>
                                <td>{{ucfirst($booking->guide->first_name) }} {{  ucfirst($booking->guide->last_name)}}</td>
                                <td>{{ucfirst($booking->tour->tour_name) ?? 'N/A'}}</td>
                                <td>{{ucfirst($booking->tour->tour_city) ?? 'N/A'}}</td>
                                <td>{{$booking->booking_by_person ?? 'N/A'}}</td>
                                <td>{{$booking->total_price_by_person ?? 'N/A'}}</td>
                                <td>{{$booking->booking_by_group ?? 'N/A'}}</td>
                                <td>{{$booking->total_price_by_group ?? 'N/A'}}</td>
                                <td>{{$booking->booking_date ?? 'N/A'}}</td>
                                <td>{{$booking->booking_start_time ?? 'N/A'}}</td>
                                <td>{{$booking->booking_end_time ?? 'N/A'}}</td>
                                <td>{{$booking->transaction_type ? 'paid' :'not-paid'}}</td>
                                <td nowrap>
                                    @if(!is_null($booking->transaction_type))
                                        @php
                                            $date_now = new DateTime();
                                            $current_date = $date_now->format('d/m/yy');
                                        @endphp
                                        @if($current_date <= $booking->booking_date)
                                            <a href="#"
                                               onclick="editTour({{ $booking }} ,'{{$booking->tour->tour_hours}}')"
                                               class="btn btn-sm btn-danger pull-left ">Edit</a>
                                        @else


                                            @if($booking->rating == null)
                                                <a href="#"
                                                   onclick="rateTour({{ $booking->id }} ,'{{$booking->tour->tour_id}}','{{ auth()->user()->id }}')"
                                                   class="btn btn-sm btn-info pull-left">Rate Now</a>
                                            @else
                                                <a href="#"
                                                   class="btn btn-sm btn-success pull-left"
                                                   style="color: white !important; background-color: lightgreen !important;">Completed!</a>
                                            @endif
                                        @endif
                                    @else
                                        <a href="#"
                                           onclick="payNow({{ $booking->id }})"
                                           class="btn btn-sm btn-primary pull-left">Pay Now</a>
                                    @endif
                                </td>
                            </tr>
                        @endif
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>

    </div>
@endsection

@push('modals')
    <div id="editbooking" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content" style="    height: 700px;">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Booking Information</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                </div>
                <div class="modal-body">
                    <form id="booking-edit-submit" method="post" action="javascript:void(0)">
                        @csrf

                        <input type="hidden" name="booking_total_hours" id="booking_total_hours" value="">
                        <input type="hidden" name="booking_id" id="booking_id" value="">
                        <div class="form-group">
                            <label for="no_of_peoples">No Of Persons:</label>
                            <input type="number" min="1" class="form-control" id="no_of_peoples"
                                   name="no_of_peoples" readonly>
                        </div>
                        <div class="form-group">
                            <label for="booking_date">Date:</label>
                            <input type="text" class="form-control" id="booking_date" name="booking_date"
                                   readonly>
                        </div>
                        <div class="form-group">
                            <label for="booking_start_time">Start Time:</label>
                            <input type="text" class="form-control" id="booking_start_time" required
                                   name="booking_start_time">
                        </div>

                        <div class="form-group">
                            <label for="booking_end_time">End Time:</label>
                            <input type="text" class="form-control" id="booking_end_time" required
                                   name="booking_end_time">
                        </div>

                        <div class="form-group">
                            <label for="total_cost">Total Cost:</label>
                            <input type="number" class="form-control" id="total_cost" value="0"
                                   readonly
                                   name="total_cost">
                        </div>

                        <div class="form-group">
                            <button type="submit" id="send_form" class="btn btn-primary text-right">Submit
                            </button>
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    {{--                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
                </div>
            </div>

        </div>
    </div>
    <div id="rateTour" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content" style="    height: 700px;">
                <div class="modal-header">
                    <h4 class="modal-title">Rate Tour</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                </div>
                <div class="modal-body">
                    <form id="rating-submit" method="post" action="javascript:void(0)">
                        @csrf

                        <input type="hidden" name="tour_id" id="rate_tour_id" value="">
                        <input type="hidden" name="booking_id" id="rate_booking_id" value="">
                        <input type="hidden" name="tourist_id" id="rate_tourist_id" value="">
                        <input type="hidden" name="rating" value="0" id="rating">
                        <div class="form-group">
                            <label for="rating">Rate Star</label>
                            <div class='rating-stars'>
                                <ul id='stars'>
                                    <li class='star' title='Poor' data-value='1'><i class='fa fa-star fa-fw'></i></li>
                                    <li class='star' title='Fair' data-value='2'><i class='fa fa-star fa-fw'></i></li>
                                    <li class='star' title='Good' data-value='3'><i class='fa fa-star fa-fw'></i></li>
                                    <li class='star' title='Excellent' data-value='4'><i class='fa fa-star fa-fw'></i>
                                    </li>
                                    <li class='star' title='WOW!!!' data-value='5'><i class='fa fa-star fa-fw'></i></li>
                                </ul>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="feedback">Feedback</label>
                            <textarea name="feedback" id="feedback" class="form-control" rows="5" cols="5"></textarea>

                        </div>
                        <div class="form-group">
                            <button type="submit" id="send_form" class="btn btn-primary text-right">Submit
                            </button>
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    {{--                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
                </div>
            </div>

        </div>
    </div>
    <div id="payKnow" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content" style="    height: 500px;">
                <div class="modal-header">
                    <h4 class="modal-title">Enter Your Credit Card Details</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                </div>
                <div class="modal-body">
                    <section class="price-area section-gap" id="price">
                        <div class="container">
                            <div class="row">

                                <div class="col-md-4 paymentImfo">
                                    <form method="POST" data-cc-on-file="false"
                                          data-stripe-publishable-key="{{ env('STRIPE_KEY') }}"
                                          id="payment-form">
                                        @csrf
                                        <input type="hidden" name="booking_id" id="pay_booking_id" value="">
                                        <input type="hidden" name="booking_tourist_id"
                                               value="{{ \Illuminate\Support\Facades\Auth::user()->id }}">
                                        <div class="row d-flex justify-content-center"
                                             style="margin-top: 14px;">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input type="text" name="name" id="card_name"
                                                           placeholder="Card Name" class="form-control"
                                                           required/>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input type="text" name="card_number" id="card_number"
                                                           placeholder="Card Number" class="form-control"
                                                           required/>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input type="text" name="cvv_number" id="cvc"
                                                           placeholder="CVV Number" class="form-control"
                                                           required/>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <div class="form-group">

                                                            <select name="expiry_month" id="exp_month"
                                                                    class="form-control" required>
                                                                <option value="" disabled>Expiry Month</option>
                                                                <option value="01">01</option>
                                                                <option value="02">02</option>
                                                                <option value="03">03</option>
                                                                <option value="04">04</option>
                                                                <option value="05">05</option>
                                                                <option value="06">06</option>
                                                                <option value="07">07</option>
                                                                <option value="08">08</option>
                                                                <option value="09">09</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <div class="form-group">

                                                            <input type="text" name="expiry_year"
                                                                   id="expiry_year"
                                                                   placeholder="Expiry Year"
                                                                   class="form-control" required/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button type="submit" id="payment-btn"
                                                        class="btn btn-primary float-right">
                                                    Pay Now
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>


                        </div>
                    </section>
                </div>
                <div class="modal-footer">
                    {{--                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
                </div>
            </div>

        </div>
    </div>
@endpush
<style>
    .bootstrap-datetimepicker-widget {

        width: 266px !important;
    }

    .col-sm-12 {
        width: 1200px !important;
    }

</style>
@push('js')





    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/fixedheader/3.1.6/js/dataTables.fixedHeader.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
    <script src=" https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/js/bootstrap-datetimepicker.min.js"></script>
    <script>
        function payNow(bookindId) {
            $('#pay_booking_id').val(bookindId);
            $('#payKnow').modal('show');
        }

        function editTour(booking, tour_hours) {

            $('#booking_id').val(booking.id);
            $('#booking_total_hours').val(tour_hours);
            if (booking.booking_by_person == null) {
                $('#no_of_peoples').val(booking.booking_by_group);
            } else {
                $('#no_of_peoples').val(booking.booking_by_person);
            }
            $('#booking_date').val(booking.booking_date);
            $('#booking_start_time').val(booking.booking_start_time);
            $('#booking_end_time').val(booking.booking_end_time);
            if (booking.booking_by_person == null) {
                $('#total_cost').val(booking.total_price_by_group);
            } else {
                $('#total_cost').val(booking.total_price_by_person);
            }
            $('#editbooking').modal('show');


        }

        function rateTour(bookingId, tourId, touristId) {

            $('#rate_booking_id').val(bookingId);
            $('#rate_tour_id').val(tourId);
            $('#rate_tourist_id').val(touristId);
            $('#rateTour').modal('show');

        }

        $(document).ready(function () {
            var table = $('#example').DataTable();

            // Bootstrap DateTimePicker v4
            $('#expiry_year').datetimepicker({
                viewMode: 'years',
                format: 'YYYY',
                pickTime: false
            });

            $('#booking_date').datetimepicker({
                format: 'DD/MM/YYYY'
            });

            $('#booking_start_time').datetimepicker({

                format: "hh:mm A",
                pickDate: false
            });
            $('#booking_end_time').datetimepicker({

                format: "hh:mm A",
                pickDate: false
            });
            $('#booking-edit-submit').submit(function (e) {
                e.preventDefault();

                var start_time = moment($("#booking_start_time").val(), "HH:mm a");
                var end_time = moment($("#booking_end_time").val(), "HH:mm a");

                var total_hours = end_time.diff(start_time, 'hours');
                var isGreater = end_time.isAfter(start_time, 'hours');

                if (isGreater) {
                    if (total_hours <= parseInt($('#booking_total_hours').val())) {
                        let form = $('#booking-edit-submit');
                        var request = $(form).serialize();

                        $.ajax({
                            type: "POST",
                            url: "{{ route('booking.edit') }}",
                            data: request,
                            dataType: "json",
                            cache: false,
                            success: function (response) {
                                if (response.status == 'success') {
                                    alert('booking updated successfully');
                                    $("#editbooking").modal('hide');
                                    window.location.reload();
                                } else {
                                    alert('Please fill all required fields.');
                                }

                            },
                            error: function () {
                                alert('Something went wrong.');
                            }
                        });
                    } else {
                        alert('You have exceed tour hours limit ' + parseInt($('#booking_total_hours').val()) + '');
                    }
                } else {
                    alert('End Time Should be Greater than start Time');
                }


            });
            $('#stars li').on('mouseover', function () {
                var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on

                // Now highlight all the stars that's not after the current hovered star
                $(this).parent().children('li.star').each(function (e) {
                    if (e < onStar) {
                        $(this).addClass('hover');
                    } else {
                        $(this).removeClass('hover');
                    }
                });

            }).on('mouseout', function () {
                $(this).parent().children('li.star').each(function (e) {
                    $(this).removeClass('hover');
                });
            });

            /* 2. Action to perform on click */
            $('#stars li').on('click', function () {
                var onStar = parseInt($(this).data('value'), 10); // The star currently selected
                var stars = $(this).parent().children('li.star');

                for (i = 0; i < stars.length; i++) {
                    $(stars[i]).removeClass('selected');
                }

                for (i = 0; i < onStar; i++) {
                    $(stars[i]).addClass('selected');
                }

                // JUST RESPONSE (Not needed)
                var ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
                $('#rating').val(ratingValue);
            });


            $('#rating-submit').submit(function (e) {
                e.preventDefault();

                let form = $('#rating-submit');
                var request = $(form).serialize();

                $.ajax({
                    type: "POST",
                    url: "{{ route('rate.now') }}",
                    data: request,
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        if (response.status == 'success') {
                            alert('Rating Save Successfully');
                            $("#rateTour").modal('hide');
                            window.location.reload();
                        } else {
                            alert('Please fill all required fields.');
                        }
                    },
                    error: function () {
                        alert('Something went wrong.');
                    }
                });
            });
        });


    </script>

    <script src="https://js.stripe.com/v2/"></script>
    <script src="{{ asset('/js/toastr.min.js') }}"></script>
    <script>

        // // Create a Stripe client.
        Stripe.setPublishableKey("pk_test_wpkJWqempchduQBeT80yxPpL00JQRMawKX");
        // //var stripe = Stripe('pk_test_16K7zw7Vw7Iyd5xjNBb9MUXS');

        $(document).ready(function () {
            $('#card_number').click(function () {
                $(this).val('');
            });

            $('#payment-form').submit(function (event) {
                $('#payment-btn').val('Processing...');

                var $form = $(this);

                // Stripe.setPublishableKey($form.data('stripe-publishable-key'));
                Stripe.card.createToken({
                    number: $('#card_number').val(),
                    cvc: $('#cvc').val(),
                    exp_month: $('#exp_month').val(),
                    exp_year: $('#expiry_year').val(),
                    name: $('#card_name').val(),
                }, stripeResponseHandler);
                return false; // submit from callback
            });
        });

        function stripeResponseHandler(status, response) {
            if (response.error) {

                $(".payment-errors").html(response.error.message);
                $(".payment-errors").addClass('alert alert-danger');
                $("html, body").animate({scrollTop: 0}, "slow");
            } else {

                var form_id = $("#payment-form");
                // token contains id, last4, and card type
                var token = response['id'];
                // insert the token into the form so it gets submitted to the server
                form_id.append("<input type='hidden' name='stripeToken' value='" + token + "' />");
                // and submit

                let form = $('#payment-form');
                var request = $(form).serialize();

                $.ajax({
                    type: "POST",
                    url: "{{ route('stripe') }}",
                    data: request,
                    dataType: "json",
                    cache: false,
                    success: function (response) {
                        if (response.flash_status == 'success') {

                            alert('Payment Done successfully');
                            $("#payKnow").modal('hide');
                            window.location.reload();
                        } else if (response.flash_status == 'error') {
                            alert(response.flash_message);
                        }

                    },
                    error: function () {
                        alert('Something went wrong.');
                    }
                });


                // document.getElementById('payment-form').submit();
            }
        }
    </script>

    <script>
            @if(Session::has('flash_message'))
        var type = "{{ Session::get('flash_status') }}";
        switch (type) {
            case 'info':
                toastr.info("{{ Session::get('flash_message') }}");
                break;

            case 'warning':
                toastr.warning("{{ Session::get('flash_message') }}");
                break;

            case 'success':
                toastr.success("{{ Session::get('flash_message') }}");
                break;

            case 'error':
                toastr.error("{{ Session::get('flash_message') }}");
                break;
        }
        @endif
    </script>

@endpush
