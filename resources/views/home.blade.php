@extends('layouts.app')
@section('content')
    {{-- Sign In Popup Form end --}}
    <div id="blur">
        <div class="container target">
            <br>
            <div class="row">
                <div class="col-md-3">
                    <!--left col-->
                    <ul class="list-group">
                        <li class="list-group-item text-muted" contenteditable="false">
                            <p class="profile_text">
                                <img class="profile_image" src="{{asset('profile_images')}}/{{Auth::user()->avatar}}"
                                     align="middle"> {{ Auth::user()->name }}
                            </p></li>
                        <li class="list-group-item text-left"><span class="pull-left"></span> Hire best tour guides
                            talent from around the world on our best,cost-efective and easy platform.
                        </li>
                        <li class="list-group-item text-left"><span class="pull-left"><strong class="blue_text">General Info:</strong></span><br><br>
                            City: {{ Auth::user()->city }}
                            <br>
                            Badge#:{{ Auth::user()->badge_number }}
                            <br>
                            Language: {{ Auth::user()->language }}<br>
                            Member Since: {{ Auth::user()->badge_number }}
                        </li>
                    </ul>
                    <br>
                    <ul class="list-group">
                        <li class="list-group-item text-muted" contenteditable="false"><strong class="blue_text">Messages</strong>
                        </li>
                        <li class="list-group-item text-muted" contenteditable="false">
                            <a href="{{route('guide.index', \Illuminate\Support\Facades\Auth::user()->id)}}" class="btn btn-primary">Messages</a>
                        </li>
                    </ul>
                    <ul class="list-group" style="overflow-y: scroll;height: 263px;margin-bottom:25px">

                        <li class="list-group-item text-left"><p class="profile_text">
                                <img class="profile_image" src="http://www.rlsandbox.com/img/profile.jpg"
                                     align="middle"> JOhn Doe
                            </p></li>
                        <li class="list-group-item text-left"><p class="profile_text">
                                <img class="profile_image" src="http://www.rlsandbox.com/img/profile.jpg"
                                     align="middle"> JOhn Doe
                            </p></li>
                        <li class="list-group-item text-left"><p class="profile_text">
                                <img class="profile_image" src="http://www.rlsandbox.com/img/profile.jpg"
                                     align="middle"> JOhn Doe
                            </p></li>
                        <li class="list-group-item text-left"><p class="profile_text">
                                <img class="profile_image" src="http://www.rlsandbox.com/img/profile.jpg"
                                     align="middle"> Paul Smith
                            </p></li>
                        <li class="list-group-item text-left">
                            <p class="profile_text">
                                <img class="profile_image" src="http://www.rlsandbox.com/img/profile.jpg"
                                     align="middle">Henry
                            </p></li>
                        <li class="list-group-item text-left"><p class="profile_text">
                                <img class="profile_image" src="http://www.rlsandbox.com/img/profile.jpg"
                                     align="middle"> Fredich <br>
                            </p>

                        </li>
                    </ul>
                </div>
                {{-- </div> --}}
                {{-- <div class="row"> --}}
                {{-- Right Column --}}
                <div class="col-md-9">
                    <div style="display:none">
                        {{$a= Auth::user()->id}};
                    </div>
                    <img style="width:100%" class="mouse_rove" src="assets/images/dashboard1.jpg" align="middle"
                         onclick="return create_tour()">
                    @foreach ($tours as $tour)
                        @if( $a == $tour->guide_id )
                            <div class="tours">
                                <?php
                                $p = array();
                                $p = json_decode($tour->tour_images);
                                ?>
                                @foreach ((array)$p as  $images)
                                    @if(!empty($images))
                                        <img class="tour_image" src="{{asset('thumbnail/images')}}/{{$images}}"
                                             align="left">
                                    @endif
                                    @break;
                                @endforeach
                                <div align="centre">
                                    <span onclick="" class="closebtn"
                                          style="    float: right;
    background-color: black;
    font-size: 25px;
    width: 3%">&times;</span>
                                    <p style="">
                                        <b>Tour Name</b>:{{ $tour->tour_name }}<br><br>
                                        <b>Tour City</b>:{{ $tour->tour_city }}<br><br>
                                        <b>Description</b>:{{ $tour->tour_description }}<br><br>
                                        <b>Tour Time</b>:{{ $tour->tour_hours }}<br><br>
                                        <a id="edit" align="right" data-id="" class="btn btn-primary"
                                           onclick="return edit_tour({{$tour}})">Edit</a>
                                        <a id="delete" align="right" data-id="" class="btn btn-danger" style="background-color: #bd2130 !important;
    border-color: #b21f2d !important;" onclick="return delete_tour({{$tour}})">Delete</a>
                                    </p>
                                </div>
                            </div>
                        @endif
                    @endforeach
                    {!! $tours->links() !!}


                </div>
            </div>

        </div>
        <br>
        <section class="footer3 cid-rQBk90RhOx" id="footer3-m">


            <div class="container">
                <div class="media-container-row content">
                    <div class="col-md-2 col-sm-4">
                        <div class="mb-3 img-logo">
                            <a href="https://mobirise.co/">
                                <img src="assets/images/fav.png" style="width: 100px" alt="Mobirise">
                            </a>
                        </div>

                    </div>
                    <div class="col-md-3 col-sm-4">
                        <p class="mb-4 mbr-fonts-style foot-title display-7">
                            RECENT NEWS
                        </p>
                        <p class="mbr-text mbr-links-column mbr-fonts-style display-7">
                            <a href="#" class="text-black">About us</a>
                            <br><a href="#" class="text-black">Services</a>
                            <br><a href="#" class="text-black">Selected Work</a>
                            <br><a href="#" class="text-black">Get In Touch</a>
                            <br><a href="#" class="text-black">Careers</a>
                        </p>
                    </div>
                    <div class="col-md-3 col-sm-4">
                        <p class="mb-4 mbr-fonts-style foot-title display-7">
                            CATEGORIES
                        </p>
                        <p class="mbr-text mbr-fonts-style mbr-links-column display-7">
                            <a href="#" class="text-black">Business</a>
                            <br><a href="#" class="text-black">Design</a>
                            <br><a href="#" class="text-black">Real life</a>
                            <br><a href="#" class="text-black">Science</a>
                            <br><a href="#" class="text-black">Tech</a>
                        </p>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <p class="mb-4 mbr-fonts-style foot-title display-7"></p>
                        <p class="mbr-text form-text mbr-fonts-style display-7"></p>


                    </div>
                </div>
                <div class="footer-lower">
                    <div class="media-container-row">
                        <div class="col-sm-12">
                            <hr>
                        </div>
                    </div>
                    <div class="media-container-row">
                        <div class="col-sm-6 copyright">
                            <p class="mbr-text mbr-fonts-style display-7">
                                © Copyright 2020 Tour Guide - All Rights Reserved
                            </p>
                        </div>
                        <div class="col-md-6">

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@push('modals')
    {{--    Create tour modal--}}
    <div id="create-tour" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content" style="    height: 1000px;">
                <div class="modal-header">
                    <h4 class="modal-title">Create Tour</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                </div>
                <div class="modal-body">
                    <form id="create_t_form" style="width: 90%" method="POST" action="javascript:void(0)"
                          enctype="multipart/form-data">
                        @csrf
                        <span id="success" style="color:red;"></span>
                        <p>
                            <input id="guide_id" type="hidden"
                                   class="w3-input w3-padding-16 w3-border @error('guide_id') is-invalid @enderror"
                                   name="guide_id" required autocomplete="guide_id" value="{{ Auth::user()->id }}"
                                   autofocus>
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                            @enderror
                        </p>
                        <p>
                            <label for="tour_name" style="color:#7A97FF"
                                   class="col-xs-4 col-form-label text-md-left">{{ __('Tour Name') }}</label>
                            <input id="tour_name" type="text"
                                   class="w3-input w3-padding-16 w3-border @error('tour_name') is-invalid @enderror"
                                   name="tour_name" required autocomplete="tour_name" autofocus>
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </p>
                        <p>
                            <label for="tour_city" style="color:#7A97FF"
                                   class="col-xs-4 col-form-label text-md-left">{{ __('Tour City') }}</label>
                            <input id="tour_city" type="text"
                                   class="w3-input w3-padding-16 w3-border @error('tour_city') is-invalid @enderror"
                                   name="tour_city" required autocomplete="tour_city" autofocus>
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </p>
                        <p>
                            <label for="tour_hours" style="color:#7A97FF"
                                   class="col-md-4 col-form-label text-md-left">{{ __('Tour Hours') }}</label>
                            <input id="tour_hours" type="text"
                                   class="w3-input w3-padding-16 w3-border @error('tour_hours') is-invalid @enderror"
                                   name="tour_hours" value="{{ old('tour_hours') }}" required autocomplete="tour_hours">

                            @error('tour_hours')
                            <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                            @enderror
                        </p>

                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" id="defaultUnchecked" name="select_payment"
                                   value="by_group">
                            <label class="custom-control-label" for="defaultUnchecked">Pricing By Group</label>
                        </div>

                        <!-- Default checked -->
                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" id="defaultChecked" name="select_payment"
                                   value="by_person" checked>
                            <label class="custom-control-label" for="defaultChecked">Pricing By Person</label>
                        </div>

                        <p>
                            <label for="tour_persons" style="color:#7A97FF"
                                   class="col-md-4 col-form-label text-md-left">{{ __('Tour Person') }}</label>
                            <input id="tour_persons" type="text"
                                   class="w3-input w3-padding-16 w3-border @error('tour_persons') is-invalid @enderror"
                                   name="tour_persons" placeholder="Enter Minimum Persons"
                                   value="{{ old('tour_persons') }}" required autocomplete="tour_persons">

                            @error('tour_persons')
                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                            @enderror
                        </p>
                        <p>
                            <label for="tour_persons" style="color:#7A97FF"
                                   class="col-md-4 col-form-label text-md-left">{{ __('Tour Person') }}</label>
                            <input id="tour_price" type="text"
                                   class="w3-input w3-padding-16 w3-border @error('tour_price') is-invalid @enderror"
                                   name="tour_price" placeholder="Enter Minimum Persons" value="{{ old('tour_price') }}"
                                   required autocomplete="tour_price">

                            @error('tour_price')
                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                            @enderror
                        </p>
                        <p>
                            <label for="tour_description" style="color:#7A97FF"
                                   class="col-md-4 col-form-label text-md-left">{{ __('Tour Description') }}</label>
                            <textarea rows="5" cols="50" id="tour_description" name="tour_description"
                                      class="w3-input w3-padding-16 w3-border @error('tour_description') is-invalid @enderror"
                                      value="{{ old('tour_description') }}" required
                                      autocomplete="tour_description"></textarea>


                            @error('tour_description')
                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                            @enderror
                        </p>
                        <p>
                            <label for="tour_images" style="color:#7A97FF"
                                   class="col-md-4 col-form-label text-md-left">{{ __('Tour Images') }}</label>
                            <input id="tour_images" type="file"
                                   class="w3-input w3-padding-16 w3-border @error('tour_images') is-invalid @enderror"
                                   name="tour_images[]" value="{{ old('tour_images') }}" required
                                   autocomplete="tour_images" multiple>

                            @error('tour_images')
                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                            @enderror
                        </p>
                        <div style="width: 25%;float: right;margin-right: 8%;margin-bottom: 2%;">
                            <button id="create_t" class="w3-button w3-light-grey w3-padding-small"
                                    style="padding: 10px !important" type="submit">
                                {{ __('Create') }}
                            </button>
                        </div>

                    </form>
                </div>

            </div>

        </div>
    </div>

    <div id="edit-tour" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content" style="height: 700px;">
                <div class="modal-header">
                    <h4 class="modal-title">Create Tour</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                </div>
                <div class="modal-body">
                    <form id="form_update" style="width: 90%" method="POST" action=" {{ route('home.update',1 )}}"
                          enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <p>

                            <input id="tour_id_edit" type="hidden"
                                   class="w3-input w3-padding-16 w3-border @error('tour_id') is-invalid @enderror"
                                   name="tour_id_edit" value="{{--{{ $tours->tour_id }}" --}} " required
                                   autocomplete="tour_id" autofocus>
                            @error('name')
                            <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
        </span>
                            @enderror
                        </p>
                        <p>
                            <label for="tour_name" style="color:#7A97FF"
                                   class="col-xs-4 col-form-label text-md-left">{{ __('Tour Name') }}</label>
                            <input id="tour_name_edit" type="text"
                                   class="w3-input w3-padding-16 w3-border @error('tour_name_edit') is-invalid @enderror"
                                   name="tour_name_edit" value="{{--{{ $tours->tour_name }}" --}} " required
                                   autocomplete="tour_name_edit" autofocus>
                            @error('tour_name_edit')
                            <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
        </span>
                            @enderror
                        </p>
                        <p>
                            <label for="tour_city" style="color:#7A97FF"
                                   class="col-xs-4 col-form-label text-md-left">{{ __('Tour City') }}</label>
                            <input id="tour_city_edit" type="text"
                                   class="w3-input w3-padding-16 w3-border @error('tour_city_edit') is-invalid @enderror"
                                   name="tour_city_edit" value="{{--{{ $tours->tour_city }}" --}} " required
                                   autocomplete="tour_city_edit" autofocus>
                            @error('tour_city_edit')
                            <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
        </span>
                            @enderror
                        </p>
                        <p>
                            <label for="tour_hours" style="color:#7A97FF"
                                   class="col-md-4 col-form-label text-md-left">{{ __('Tour Hours') }}</label>
                            <input id="tour_hours_edit" type="text"
                                   class="w3-input w3-padding-16 w3-border @error('tour_hours_edit') is-invalid @enderror"
                                   name="tour_hours_edit" value="{{--{{ $tours->tour_hours }}" --}} " required
                                   autocomplete="tour_hours_edit">

                            @error('tour_hours_edit')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </p>
                        <p>
                            <label for="tour_description" style="color:#7A97FF"
                                   class="col-md-4 col-form-label text-md-left">{{ __('Tour Description') }}</label>
                            <textarea rows="5" cols="50" id="tour_description_edit" name="tour_description_edit"
                                      class="w3-input w3-padding-16 w3-border @error('tour_description_edit') is-invalid @enderror"
                                      value=" {{--{{ $tours->tour_description }}"--}}" required
                                      autocomplete="tour_description_edit"></textarea>

                            @error('tour_description_edit')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </p>


                        <div style="width: 25%;
    float: right;
    margin-right: 8%;
    margin-bottom: 2%;">
                            <button id="update" class="w3-button w3-light-grey w3-padding-small"
                                    style="padding: 10px !important" type="submit">
                                {{ __('Update') }}
                            </button>
                        </div>

                    </form>
                </div>

            </div>

        </div>
    </div>

    <div id="delete-tour" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content" style="height: 300px;">
                <div class="modal-header">
                    <h3 class="modal-title" style="font-weight:200;color:#7A97FF;"><b>Do you
                            want to delete this tour?</b></h3>
                    <button type=" button" class="close" data-dismiss="modal">&times;</button>

                </div>
                <div class="modal-body">
                    <form id="form_Delete" style="width: 90%" method="POST" action="{{ route('home.destroy',':id' )}}"
                          enctype="multipart/form-data">
                        @csrf
                        @method('DELETE')
                        <p>

                            <input id="tour_id_delete" type="hidden"
                                   class="w3-input w3-padding-16 w3-border @error('tour_id_delete') is-invalid @enderror"
                                   name="tour_id_delete" value="{{--{{ $tours->tour_id }}" --}} " required
                                   autocomplete="tour_id_delete" autofocus>
                            @error('tour_id_delete')
                            <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
        </span>
                            @enderror
                        </p>

                        <a onclick="cancel('delete')" align="right" data-id="" class="btn btn-primary">Cancel</a>

                        <button id="delete" class="btn btn-primary" type="submit">{{ __('Delete') }}</button>
                    </form>
                </div>

            </div>

        </div>
    </div>
@endpush
@push('js')
    <script>
        function create_tour() {
            $('#create-tour').modal('show');
        }

        $("#defaultUnchecked").click(function () {
            $("#tour_persons").attr("placeholder", "Enter Maximum Persons");
            $("#tour_price").attr("placeholder", "Enter Price For Group");
        });
        $("#defaultChecked").click(function () {
            $("#tour_persons").attr("placeholder", "Enter Minimum Persons");
            $("#tour_price").attr("placeholder", "Enter Price Per Person");
        });

        function edit_tour(tour) {


            $('#tour_id_edit').val(tour.tour_id);
            $('#tour_name_edit').val(tour.tour_name);
            $('#tour_description_edit').val(tour.tour_description);
            $('#tour_hours_edit').val(tour.tour_hours);
            $('#tour_city_edit').val(tour.tour_city);


            $('#edit-tour').modal('show');

        }

        function delete_tour(tour) {

            $('#tour_id_delete').val(tour.tour_id);
            $('#delete-tour').modal('show');

        }

        function cancel(type) {
            $('#delete-tour').modal('hide');
        }

        $('#create_t').click(function (e) {

            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })
            let image_upload = new FormData();
            let TotalImages = $('#tour_images')[0].files.length;  //Total Images
            let images = $('#tour_images')[0];
            for (let i = 0; i < TotalImages; i++) {
                image_upload.append('images' + i, images.files[i]);
            }
            image_upload.append('TotalImages', TotalImages);
            image_upload.append('guide_id', $('#guide_id').val());
            image_upload.append('tour_name', $('#tour_name').val());
            image_upload.append('tour_city', $('#tour_city').val());
            image_upload.append('tour_hours', $('#tour_hours').val());
            if ($('#defaultUnchecked').is(':checked')) {
                image_upload.append('select_payment', $('#defaultChecked').val());
            } else {
                image_upload.append('select_payment', $('#defaultUnchecked').val());
            }

            image_upload.append('tour_persons', $('#tour_persons').val());
            image_upload.append('tour_price', $('#tour_price').val());
            image_upload.append('tour_description', $('#tour_description').val());
            $.ajax({
                type: "POST",
                url: "{{ route('create.tour') }}",
                data: image_upload,
                contentType: false,
                processData: false,
                cache: false,
                success: function (response) {

                    if (response.error == '4151') {
                        alert("Sorry Can't add");
                        $('#create-tour').modal('hide');
                    } else {
                        alert('yes');
                        window.location.reload();
                    }

                },
                error: function () {
                    alert('Something went wrong.');
                }
            });

        });
    </script>



    <script src="{{ asset('js/script1.js') }}"></script>
@endpush
{{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>--}}

