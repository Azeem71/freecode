<head>
    <meta charset="UTF-8">
    <!-- Favicon below-->
    <link rel="shortcut icon" href="{{ asset('assets/images/fav.png')}}" type="image/x-icon">
    <meta name="description" content="">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('assets/web/assets/mobirise-icons/mobirise-icons.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/bootstrap/css/bootstrap-grid.min.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/bootstrap/css/bootstrap-reboot.min.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/tether/tether.min.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/socicon/css/styles.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/theme/css/style.css')}}">
    <link href="{{ asset('assets/fonts/style.css')}}" rel="stylesheet">
    <link rel="preload" as="style" href="{{ asset('assets/mobirise/css/mbr-additional.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/mobirise/css/mbr-additional.css')}}" type="text/css">

    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <title>Home</title>
    <link rel="stylesheet" href="{{ asset('css/style.css')}}">
    <link href="{{ asset('assets/fonts/style.css')}}" rel="stylesheet">
    <link rel="preload" as="style" href="{{ asset('assets/mobirise/css/mbr-additional.css')}}">


    <style>
        @media screen and (max-width: 600px) {
            .links > a {
                color: #636b6f;
                padding: 10px 7px !important;
                font-size: 11px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }
        }
    </style>
</head>
<body style="background-color:#FCFCFC;">

{{-- Sign In Popup Form start--}}
<div class="row">
    <div id="myModal" class="modalcustom">

        <!-- Modal content -->
        <div class="col-xs-6 modal-content">
            <span class="close" align="right">&times;</span>


            <!-- Sidebar with image -->
            <nav class="col-xs-3 mobile_change">

                <div class="bgimg"></div>
            </nav>

            <div class="col-xs-3" style="margin-left:39%">

                <!-- Menu icon to open sidebar -->

                <!-- Header -->
                <header class="w3-container w3-center" id="home">
                    <h1 class="w3-jumbo" style="font-weight:200;color:#7A97FF;font-size: 50px!important;"><b>Sign In</b>
                    </h1>
                </header>

                <div class="row">
                    <div class="column1">
                        <img src="/assets/images/facebook.png" alt="facebook" style="width:100%">
                    </div>
                    <div class="column2">
                        <img src="/assets/images/google.png" alt="google" style="width:100%">
                    </div>

                </div>
                <!-- SignIn -->
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <p>
                        <label for="name" style="color:#7A97FF"
                               class="col-xs-4 col-form-label text-xs-left">{{ __('E-Mail Address') }}</label>
                        <input id="email" type="email"
                               class="w3-input w3-padding-16 w3-border @error('email') is-invalid @enderror"
                               name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                        @error('email')
                        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
            </span>
                        @enderror
                    </p>

                    <p>
                        <label for="name" style="color:#7A97FF"
                               class="col-xs-4 col-form-label text-xs-left">{{ __('Password') }}</label>
                        <input id="password" type="password"
                               class="w3-input w3-padding-16 w3-border @error('password') is-invalid @enderror"
                               name="password" required autocomplete="current-password">
                        @error('password')
                        <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
        </span>
                        @enderror

                    </p>
                    <div class="form-group row">
                        <div class="col-xs-6 offset-xs-4">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="remember"
                                       id="remember" {{ old('remember') ? 'checked' : '' }}>

                                <label class="form-check-label" for="remember">
                                    {{ __('Remember Me') }}
                                </label>
                            </div>
                        </div>
                    </div>
                    <div style="width:25%;float:left">
                        <button class="w3-button w3-light-grey w3-padding-small"
                                style="padding: 10px !important;margin-top: 12%" type="submit">
                            {{ __('Login') }}
                        </button>
                    </div>
                    @if (Route::has('password.request'))
                        <a class="btn btn-link" href="{{ route('password.request') }}">
                            {{ __('Forgot Your Password?') }}
                        </a>
                    @endif

                </form>
                <!-- End Contact Section -->
            </div>


            <!-- END PAGE CONTENT -->
        </div>


    </div>
</div>

{{-- Sign In Popup Form end --}}


{{-- Sign Up Popup Form start--}}
<div class="row">
    <div id="myModal1" class="modalcustom">

        <!-- Modal content -->
        <div class="col-xs-6 modal-content">
            <span class="close" align="right">&times;</span>


            <!-- Sidebar with image -->
            <nav class="col-xs-3 mobile_change">

                <div class="bgimg"></div>
            </nav>

            <div class="col-xs-3" style="margin-left:39%;overflow-y:scroll">

                <!-- Menu icon to open sidebar -->

                <!-- Header -->
                <header class="w3-container w3-center" id="home">
                    <h1 class="w3-jumbo" style="font-weight:200;color:#7A97FF;font-size: 50px!important;"><b>Sign Up</b>
                    </h1>
                </header>

                <div class="row">
                    <div class="column1">
                        <img src="{{ asset('assets/images/facebook.png')}}" alt="facebook" style="width:100%">
                    </div>
                    <div class="column2">
                        <img src="{{ asset('assets/images/google.png')}}" alt="google" style="width:100%">
                    </div>

                </div>
                <!-- SignIn -->
                <form method="POST" action="{{ route('register') }}">
                    @csrf
                    <p>
                        <label for="name" style="color:#7A97FF"
                               class="col-xs-4 col-form-label text-md-left">{{ __('Name') }}</label>
                        <input id="name" type="text"
                               class="w3-input w3-padding-16 w3-border @error('name') is-invalid @enderror" name="name"
                               required autocomplete="name" autofocus>
                        @error('name')
                        <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
        </span>
                        @enderror
                    </p>
                    <p>
                        <label for="name" style="color:#7A97FF"
                               class="col-xs-4 col-form-label text-xs-left">{{ __('E-Mail Address') }}</label>
                        <input id="email" type="email"
                               class="w3-input w3-padding-16 w3-border @error('email') is-invalid @enderror"
                               name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                        @error('email')
                        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
            </span>
                        @enderror
                    </p>

                    <p>
                        <label for="name" style="color:#7A97FF"
                               class="col-xs-4 col-form-label text-xs-left">{{ __('Password') }}</label>
                        <input id="password" type="password"
                               class="w3-input w3-padding-16 w3-border @error('password') is-invalid @enderror"
                               name="password" required autocomplete="current-password">
                        @error('password')
                        <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
        </span>
                        @enderror

                    </p>

                    <p>
                        <label for="name" style="color:#7A97FF"
                               class="col-md-4 col-form-label text-md-left">{{ __('Confirm Password') }}</label>
                        <input id="password-confirm" type="password"
                               class="w3-input w3-padding-16 w3-border @error('password') is-invalid @enderror"
                               name="password_confirmation" value="{{ old('password') }}" required
                               autocomplete="new-password">

                        @error('password')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </p>

                    <div style="width: 25%;
    float: right;
    margin-right: 8%;
    margin-bottom: 2%;">
                        <button class="w3-button w3-light-grey w3-padding-small" style="padding: 10px !important"
                                type="submit">
                            {{ __('Register') }}
                        </button>
                    </div>
                    <div style="float:left">
                        Already have an account?

                        <a href="#" onclick="return signin()" class="auth links">Sign In</a></div>
                </form>
                <!-- End Contact Section -->
            </div>


            <!-- END PAGE CONTENT -->
        </div>


    </div>
</div>

{{-- Sign In Popup Form end --}}
<div class='row'>
    @yield('form')
</div>


@stack('js')
</body>
